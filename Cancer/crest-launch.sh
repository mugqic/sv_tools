#~/bin/bash


if [ $# != 6 ]; then
echo "usage: $0 starting_port chromosome sample_name tumor_name normal_name work_path"
exit
fi

STARTING_PORT=$1
CHR=$2
SNAME=$3
TNAME=$4
NNAME=$5
WORK_PATH=$6

set -o
set -e

mkdir -p ${WORK_PATH}/pairedSnps/${SNAME}/crest
Port=$STARTING_PORT
while netstat -atwn | grep "^.*:${Port}.*:\*\s*LISTEN\s*$";do Port=$(( ${Port} + 1 )) ;done

gfServer -canStop start localhost ${Port} /lb/project/mugqic/genomes/human_g1k_v37/human_g1k_v37.2bit &
echo "Wait for the server to start"
sleep 600;
echo "Ready to go"
CREST.pl -f ${WORK_PATH}/${TNAME}/crest/${TNAME}.sorted.dup.bam.cover -d ${WORK_PATH}/${TNAME}/${TNAME}.sorted.dup.bam -g ${WORK_PATH}/${NNAME}/${NNAME}.sorted.dup.bam --ref_genome /lb/project/mugqic/genomes/human_g1k_v37/human_g1k_v37.fasta -t /lb/project/mugqic/genomes/human_g1k_v37/human_g1k_v37.2bit -r ${CHR} --blatserver localhost --blatport ${Port} -o ${WORK_PATH}/pairedSnps/${SNAME}/crest/ --prefix ${SNAME}.${CHR}

echo "Stop the server"
gfServer stop localhost ${Port}
echo "Server stopped"
