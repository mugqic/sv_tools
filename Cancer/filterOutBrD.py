#!/usr/bin/python

### Mathieu Bourgey (2011/12/01) - mbourgey@genomequebec.com
### filter out BreakDancer SV results 


import os
import sys
import string
import getopt
import re


def getarg(argument):
	strd=""
	fil=""
	Nna=""
	Tna=""
	nor=5
	tum=5
	optli,arg = getopt.getopt(argument[1:],"f:n:b:t:c:o:s:h",['file','normal','blood','tumor','cancer','output','sample','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			fil=str(value)
		if option in ("-n","--normal"):
			nor=int(value)
		if option in ("-b","--blood"):
			Nna=str(value)
		if option in ("-t","--tumor"):
			tum=int(value)
		if option in ("-c","--cancer"):
			Tna=str(value)
		if option in ("-s","--sample"):
			strd=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(fil) :
                sys.exit("Error - BreakDancer result file not found:\n"+str(fil))
	if Nna == "" or Tna == "" :
                sys.exit("Error - BreakDancer bam file not found")
	if strd > 1 and  strd < 0 :
		sys.exit("Error - Strand filter unknown (0 - 1):\n"+str(strd))
	return fil, nor, Nna, tum, Tna, strd, out



##
def usage():
	print "\n---------------------------------------------------------------------------------"
	print "filterOutBrD.py filter out BreakDancer SV results"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : filterOutBrD.py [option] "
	print "       -f :        BreakDancer result file"
	print "       -n :        normal min read support"
	print "       -b :        normal bam file"
	print "       -t :        tumoral min read support"
	print "       -c :        tumoral bam file"
	print "       -o :        output basename"
	print "       -s :        sanple Base name"
	print "       -h :        this help \n"


def main():
	fil, nor, norFP, tum, tumFP, saN, outb = getarg(sys.argv)
	testType=["DEL","INS","INV","ITX","CTX","INSTX","Unknown"]
	outPose=[1,2,3,5,5,6,7]
	outType=["small_INDEL","DEL","INS","INV","DUP","TRA","INS_TRA","UNK"]
	NTcount=[0,0,0,0,0,0,0,0]
	Ncount=[0,0,0,0,0,0,0,0]
	Tcount=[0,0,0,0,0,0,0,0]
	Acount=[0,0,0,0,0,0,0,0]
	outF=open(outb+".filteredSV.txt",'w')
	outF.write("Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\n")
	outID=open(outb+".INDEL.txt",'w')
	outID.write("Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\n")
	outS=open(outb+".Summary_count.txt",'w')
	fi=open(fil,'r')
	li=fi.readline()
	while li != "" :
		if li[0] != "#" :
			writeL=False
			ci=li.split()
			#print ci
			outL="BrD\t"+saN+"\t"+ci[0]+"\t"+ci[1]+"\t"+ci[4]+"\t"+ci[3]+"\t"+str(abs(int(ci[7])))
			info=ci[10]
			infS=info.split(":")
			fiI=infS[0].split("|")
			if (ci[0] != ci[3]) and ci[6] == "INS" :
				ci[6]="INSTX"
			postmp=testType.index(ci[6])
			pos=outPose[postmp]
			if postmp <= 1 and abs(int(ci[7])) <= 50 :
				pos=0
			typeSV=outType[pos]
			if len(infS) == 1 :
				if fiI[0] == tumFP :
					if int(fiI[1]) >= tum :
						writeL=True
						Tcount[pos]=Tcount[pos]+1
						supR="0/"+fiI[1]
						state="T"
				elif fiI[0] == norFP :
					if int(fiI[1]) >= nor :
						writeL=True
						Ncount[pos]=Ncount[pos]+1
						supR=fiI[1]+"/0"
						state="N"
			else :
				fiII=infS[1].split("|")
				if fiI[0] == tumFP :
					supR=fiII[1]+"/"+fiI[1]
					Inum=tum
					IInum=nor
				else :
					supR=fiI[1]+"/"+fiII[1]
					IInum=tum
					Inum=nor
				if int(fiI[1]) >= Inum and int(fiII[1]) >= IInum :
					writeL=True
					NTcount[pos]=NTcount[pos]+1
					state="NT"
				elif not (int(fiI[1]) < Inum and int(fiII[1]) < IInum) :
					writeL=True
					Acount[pos]=Acount[pos]+1
					state="A"
			if writeL and pos < 7 :
				if typeSV != "small_INDEL" :
					outF.write(outL+"\t"+typeSV+"\t"+supR+"\t"+state+"\n")
				else :
					outID.write(outL+"\t"+typeSV+"\t"+supR+"\t"+state+"\n")
		li=fi.readline()
	fi.close()
	outF.close()
	outID.close()
	outS.write("Total\tINDEL\tDEL\tINS\tINV\tDUP\tTRA\tINS_TRA\n")
	outS.write(str(sum(Tcount))+"\t"+str(Tcount[0])+"\t"+str(Tcount[1])+"\t"+str(Tcount[2])+"\t"+str(Tcount[3])+"\t"+str(Tcount[4])+"\t"+str(Tcount[5])+"\t"+str(Tcount[6])+"\n")
	outS.write(str(sum(Ncount))+"\t"+str(Ncount[0])+"\t"+str(Ncount[1])+"\t"+str(Ncount[2])+"\t"+str(Ncount[3])+"\t"+str(Ncount[4])+"\t"+str(Ncount[5])+"\t"+str(Ncount[6])+"\n")
	outS.write(str(sum(NTcount))+"\t"+str(NTcount[0])+"\t"+str(NTcount[1])+"\t"+str(NTcount[2])+"\t"+str(NTcount[3])+"\t"+str(NTcount[4])+"\t"+str(NTcount[5])+"\t"+str(NTcount[6])+"\n")
	outS.write(str(sum(Acount))+"\t"+str(Acount[0])+"\t"+str(Acount[1])+"\t"+str(Acount[2])+"\t"+str(Acount[3])+"\t"+str(Acount[4])+"\t"+str(Acount[5])+"\t"+str(Acount[6])+"\n")
	outS.close()

main()

	    
	
	





