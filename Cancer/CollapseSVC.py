#!/usr/bin/python

### Mathieu Bourgey (2012/03/23) - mbourgey@genomequebec.com
### filter out BreakDancer SV results 


import os
import sys
import string
import getopt
import re


def getarg(argument):
	fil=""
	optli,arg = getopt.getopt(argument[1:],"f:o:h",['file','output','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			fil=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(fil) :
                sys.exit("Error - input file not found:\n"+str(fil))
	return fil, out



##
def usage():
	print "\n---------------------------------------------------------------------------------"
	print "CollapseSVC.py collapse SV results that overlap"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : CollapseSVC.py [option] "
	print "       -f :        call file"
	print "       -o :        output basename"
	print "       -s :        sanple Base name"
	print "       -h :        this help \n"


def main():
	fil, outP = getarg(sys.argv)
	fi=open(fil,'r')
	li=fi.readline()
	out=open("tmpColl.txt",'w')
	alrM=[]
	OCst={}
	while li != "" :
		ci=li.split()
		infoS=ci[3].split(";")
		if len(infoS) > 1 :
			osT=[]
			numT=[]
			rsL=[]
			## need to collapse
			for i in range(0,len(infoS),1) :
				infoI=infoS[i].split(",")
				osT.append(infoI[6])
				numT.append(infoI[0])
				rsL.append(infoI[5])
			rs=rsL[0]
			ns=numT[0]		
			if numT[0] in alrM :
				alf=True
				if OCst[numT[0]] != osT[0] :
					if OCst[numT[0]] ==  "A" or osT[0] == "A" :
						old="A"
					else :
						old="NT"
			else :
				old=osT[0]
				alrM.append(numT[0])
			for i in range(1,len(osT),1) :
				rs=rs+":"+rsL[i]
				ns=ns+":"+numT[i]
				if osT[i] != old :
					if osT[i] == "A" or old == "A" :
						old="A"
					else :
						old="NT"
				if numT[i] in alrM :
					if OCst[numT[i]] != osT[i] :
						if OCst[numT[i]] ==  "A" or osT[i] == "A" :
							old="A"
						else :
							old="NT"
			for i in range(0,len(osT),1) :
				if not numT[i] in alrM :
					alrM.append(numT[i])
				OCst[numT[i]]=old
		else :
			infoI=infoS[0].split(",")
			old=infoI[6]
			rs=infoI[5]
			ns=infoI[0]
			if infoI[0] in alrM :
				if infoI[6] != OCst[infoI[0]] :
					if OCst[infoI[0]] ==  "A" or infoI[6] == "A" :
						old="A"
					else :
						old="NT"
			alrM.append(infoI[0])
			OCst[infoI[0]]=old
		out.write(ci[0]+"\t"+ci[1]+"\t"+ci[2]+"\t"+ns+","+infoI[1]+","+infoI[2]+","+str(int(ci[2])-int(ci[1]))+","+infoI[4]+","+rs+","+old+"\n")
		li=fi.readline()
	out.close()
	fi.close()
	fi=open("tmpColl.txt",'r')
	out=open(outP,'w')
	li=fi.readline()
	cpt=1
	reNUm={}
	while li != "" :
		ci=li.split()
		infoI=ci[3].split(",")
		nu=infoI[0].split(":")
		if infoI[6] != OCst[nu[0]] :
			if OCst[nu[0]] ==  "A" or infoI[6] == "A" :
				infoI[6]="A"
				OCst[nu[0]]="A"
			else :
				infoI[6]="NT"
				OCst[nu[0]]="NT"
		chgN=False
		for i in nu :
			if reNUm.has_key(i) :
				chgN=True
				repCpt=reNUm[i]
		if chgN :
			for i in nu :
				reNUm[i]=repCpt
			infoI[0]=str(repCpt)
		else :
			infoI[0]=str(cpt)
			cpt=cpt+1
		out.write(ci[0]+"\t"+ci[1]+"\t"+ci[2]+"\t"+infoI[0]+","+infoI[1]+","+infoI[2]+","+infoI[3]+","+infoI[4]+","+infoI[5]+","+infoI[6]+"\n")
		li=fi.readline()
	out.close()
	fi.close()
	os.system("rm tmpColl.txt")
main()

	    
	
	





