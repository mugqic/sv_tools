#!/usr/bin/python

### Mathieu Bourgey (2012/09/13) - mbourgey@genomequebec.com
### generate bed bin of a given size from a chromosome length file

import os
import sys
import string
import getopt
import re



def getarg(argument):
	fil=""
	out=""
	si=100
	optli,arg = getopt.getopt(argument[1:],"f:s:o:h",['file','size','output','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			fil=str(value)
		if option in ("-s","--size"):
			si=int(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(fil) :
		usage()
		sys.exit("Error - chromosome length file not found:\n"+str(fil))
        if out == "" :
		usage()
		sys.exit("Error - no output file location provided")
	return fil, si, out



def usage():
	print "\n-------------------------------------------------------------------------------------------"
	print "getBinBed.py generate a bed track of genomic bin based on a from a reference fasta file"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "--------------------------------------------------------------------------------------------\n"
	print "USAGE : getBinBed.py [option] "
	print "       -f :        reference fasta file"
	print "       -s :        bin size"
	print "       -o :        output file"
	print "Optional arguments: "
	print "       -h :        this help \n"

def main():
	fiP, siz, outP  = getarg(sys.argv)
	print "chromosome length file: "+fiP
	print "output file: "+outP
	print "bin size: "+str(siz)
	print "Generating chromosome length..."
	os.system("/lb/project/mugqic/software/tools/fastalength "+fiP+" | grep -v \"GL\" | grep -v \"random\" | grep -v \"Un\" > "+fiP+".length") 
	print "DONE" 
	fi=open(fiP+".length",'r')
	out=open(outP,'w')
	li=fi.readline()
	while li != "" :
		st=1
		co=li.split()
		en=int(co[0])
		ch=str(co[1])
		print "Generating bed bins for "+ch+" ..."
		while st < en :
			out.write(ch+"\t"+str(st)+"\t"+str(st+(siz-1))+"\n")
			old=st
			st=old+siz
		out.write(ch+"\t"+str(old)+"\t"+str(en)+"\n")
		print "DONE"
		li=fi.readline()
	out.close()
	fi.close()

main()


	
	
	
	