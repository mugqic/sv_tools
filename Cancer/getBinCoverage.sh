#!/bin/sh

# Usage: getBinCoverage.sh bamfile($1) bamfile($2)binBed.file($2) ref($3) outputfile($4)

tmpdep=""
for i in `awk ' {print $1} ' $2 | sort -u ` 
do
	grep "^$i" $2 > tmp.$i.bed
	echo "java -Xmx15g -jar /lb/project/mugqic/software/tools/GenomeAnalysisTK/GenomeAnalysisTK.jar -T DepthOfCoverage -I $1 -L tmp.$i.bed -R $3 -o $4.$i -omitLocusTable -omitBaseOutput --omitDepthOutputAtEachBase --logging_level ERROR" | msub -d `pwd` -V -l walltime=5:00:0 -q sw -l nodes=1:ppn=6 -j oe -o DoC.$i.out -N DoC_perChr_$i -m ae -M mb.guillimin@gmail.com
	echo "cat  $4.$i >>  $4" | msub -d `pwd` -V  -q sw -l nodes=1:ppn=1 j oe -o mergeDoC.$i.out -N mergeDoC -W depend=afterok:DoC_perChr_${i}${tmpdep} -m ae -M mb.guillimin@gmail.com
	tmpdep=":mergeDoC.$i"
done



