#!/usr/bin/python

### Mathieu Bourgey (2012/09/26) - mbourgey@genomequebec.com
### Get reccuremce SV list 


import os
import sys
import string
import getopt
import re

class genRec :
	def __init__(self) :
		self.sample=[]
		self.cover=[]
		self.disrupt=[]
		self.utr=[]
		self.total=[]
		self.DUP=[]
		self.DEL=[]
		self.TRA=[]
		self.INS=[]
		self.INV=[]
		self.DGV=0
		self.chro=""
		

def getarg(argument):
	bd=""
	pi=""
	dn=""
	out=""
	optli,arg = getopt.getopt(argument[1:],"b:p:d:o:h",['BreakD','pindel','DNAC','output','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-b","--BreakD"):
			bd=str(value)
		if option in ("-p","--pindel"):
			pi=str(value)
		if option in ("-d","--DNAC"):
			dn=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	if not os.path.exists(bd) and not bd == "none" :
		sys.exit("Error - Breakdancer file not found:\n"+str(bd))
	if not os.path.exists(pi) and not pi == "none" :
		sys.exit("Error - Pindel file not found:\n"+str(pi))
	if not os.path.exists(dn) and not  dn == "none" :
		sys.exit("Error - DNAC file not found:\n"+str(dn))
	return bd, pi, dn, out

def usage():
	print "\n---------------------------------------------------------------------------------"
	print "GetReccurenceSV.py use SV filterOut results and return a recurence gene list "
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : GetReccurenceSV.py [option] "
	print "       -b :        breakdancer filtered SV file"
	print "       -p :        Pindel filtered SV file"
	print "       -d :        DNAC filtered SV file"
	print "       -o :        output basename"
	print "       -h :        this help \n"


def getGeneInfo(x):
	dico={}
	g=open(x,'r')
	l=g.readline()
	while l != "" : 
		c=l.split()
		if c[11] != "." or c[12] != "." or c[13] != "." :
			sa=c[1]
			if c[10] != "NA" :
				dgv=int(c[10])
			else :
				dgv=-1
			typ=c[7]
			if c[2] != c[5]:
				ch=c[2]+"_"+c[5]
			else :
				ch=c[2]
			for j in range(11,13,1):
				if c[j] != "." :
					gli=c[j].split(":")
					for i in range(0,len(gli)-1,1) :
						if not (dico.has_key(gli[i])) :
							dico[gli[i]]=genRec()
						dico[gli[i]].sample.append(sa)
						dico[gli[i]].chro=ch
						if j == 11 :
							dico[gli[i]].cover.append(sa)
						elif j == 12 :
							dico[gli[i]].disrupt.append(sa)
						else :
							dico[gli[i]].utr.append(sa)
						if typ == "DEL": 
							dico[gli[i]].DEL.append(sa)
						elif typ == "DUP": 
							dico[gli[i]].DUP.append(sa)
						elif typ == "INS": 
							dico[gli[i]].INS.append(sa)
						elif typ == "INV": 
							dico[gli[i]].INV.append(sa)
						elif typ == "TRA": 
							dico[gli[i]].TRA.append(sa)
						if dico[gli[i]].DGV < dgv :
							dico[gli[i]].DGV=dgv
		l=g.readline()
	g.close()
	return dico

def mergeGeneInfo(x,y):
	dico={}
	kx=x.keys()
	for i in kx :
		dico[i]=x[i]
		if y.has_key(i):
			if dico[i].DGV < y[i].DGV :
				dico[i].DGV = y[i].DGV
			for j in range(0,len(y[i].sample)-1,1) :
				dico[i].sample.append(y[i].sample[j])
			if len(y[i].cover) >= 1 :
				for j in range(0,len(y[i].cover)-1,1) :
					dico[i].cover.append(y[i].cover[j])
			if len(y[i].disrupt) >= 1 :
				for j in range(0,len(y[i].disrupt)-1,1) :
					dico[i].disrupt.append(y[i].disrupt[j])
			if len(y[i].utr) >= 1 :
				for j in range(0,len(y[i].utr)-1,1) :
					dico[i].utr.append(y[i].utr[j])
	yx=y.keys()
	for i in yx :
		if not (x.has_key(i)):
			dico[i]=y[i]
	return dico

def getOut(x): 
	if len(x.DEL) >= 1 :
		delO=str(len(set(x.DEL)))+";"+":".join(list(set(x.DEL)))
	else :
		delO="0"
	if len(x.DUP) >= 1 :
		dupO=str(len(set(x.DUP)))+";"+":".join(list(set(x.DUP)))
	else :
		dupO="0"
	if len(x.INS) >= 1 : 
		insO=str(len(set(x.INS)))+";"+":".join(list(set(x.INS)))
	else :
		insO="0"
	if len(x.INV) >= 1 :
		invO=str(len(set(x.INV)))+";"+":".join(list(set(x.INV)))
	else :
		invO="0"
	if len(x.TRA) >= 1 :
		traO=str(len(set(x.TRA)))+";"+":".join(list(set(x.TRA)))
	else :
		traO="0"
	return delO, dupO, insO, invO, traO


	
def main():
	bdp, pip, dnp, out = getarg(sys.argv)
	#get BrD reclist
	if bdp != "none" :
		GeneB=getGeneInfo(bdp)
	else :
		GeneB={}
	#get Pindel reclist
	if pip != "none" :
		GeneP=getGeneInfo(pip)
	else :
		GeneP={}
	#get DNAC reclist
	if dnp  != "none" :
		GeneD=getGeneInfo(dnp)
	else :
		GeneD={}
	#merge BrD and Pindel reclist
	GeneBP=mergeGeneInfo(GeneB,GeneP)
	#merge BrD_Pindel and DNAC reclist
	GeneBDP=mergeGeneInfo(GeneBP,GeneD)
	kall=GeneBDP.keys()
	outF=open(out,'w')
	outNull="0\t0\t0\t0\t.\t.\t.\t.\t.\t."
	if pip == "none" and dnp  == "none" and bdp != "none" :
		outF.write("Gene\tchromosome\tBrD\tBrD_cover\tBrD_disrupt\tBrD_utr\tDEL\tDUP\tINS\tINV\tTRA\tBrD_sample\n")
		for i in kall :
			#genreate output string for all calls
			#add BrD only
			if GeneB.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneB[i])
				outST=i+"\t"+GeneB[i].chro+"\t"+str(len(set(GeneB[i].sample)))+"\t"+str(len(set(GeneB[i].cover)))+"\t"+str(len(set(GeneB[i].disrupt)))+"\t"+str(len(set(GeneB[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneB[i].sample)))
			else :
				outST=outST+"\t"+outNull
			outF.write(outST+"\n")
	elif pip == "none" and dnp  != "none" and bdp == "none" :
		outF.write("Gene\tCNV\tCNV_cover\tCNV_disrupt\tCNV_utr\tDEL\tDUP\tINS\tINV\tTRA\tCNV_sample\n")
		for i in kall :
			if GeneD.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneD[i])
				outST=i+"\t"+GeneD[i].chro+"\t"+str(len(set(GeneD[i].sample)))+"\t"+str(len(set(GeneD[i].cover)))+"\t"+str(len(set(GeneD[i].disrupt)))+"\t"+str(len(set(GeneD[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneD[i].sample)))
			else :
				outST=outST+"\t"+outNull
			outF.write(outST+"\n")
	elif pip != "none" and dnp  == "none" and bdp == "none" :
		outF.write("Gene\tPI\tPI_cover\tPI_disrupt\tPI_utr\tDEL\tDUP\tINS\tINV\tTRA\tPI_sample\n")
		for i in kall :
			if GeneP.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneP[i])
				outST=i+"\t"+GeneP[i].chro+"\t"+str(len(set(GeneP[i].sample)))+"\t"+str(len(set(GeneP[i].cover)))+"\t"+str(len(set(GeneP[i].disrupt)))+"\t"+str(len(set(GeneP[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneP[i].sample)))
			else :
				outST=outST+"\t"+outNull
			outF.write(outST+"\n")
	else  :
		outF.write("Gene\tDGV_Hit\tCNV_SV\tCNV_SV_cover\tCNV_SV_disrupt\tCNV_SV_utr\tDEL\tDUP\tINS\tINV\tTRA\tCNV_SV_sample\tCNV\tCNV_cover\tCNV_disrupt\tCNV_utr\tDEL\tDUP\tINS\tINV\tTRA\tCNV_sample\tSV\tSV_cover\tSV_disrupt\tSV_utr\tDEL\tDUP\tINS\tINV\tTRA\tSV_sample\t BrD\tBrD_cover\tBrD_disrupt\tBrD_utr\tDEL\tDUP\tINS\tINV\tTRA\tBrD_sample\tPI\tPI_cover\tPI_disrupt\tPI_utr\tDEL\tDUP\tINS\tINV\tTRA\tPI_sample\n")
		for i in kall :
			#genreate output string for all calls
			delO, dupO, insO, invO, traO = getOut(GeneBDP[i])
			outST=i+"\t"+str(len(set(GeneBDP[i].sample)))+"\t"+str(len(set(GeneBDP[i].cover)))+"\t"+str(len(set(GeneBDP[i].disrupt)))+"\t"+str(len(set(GeneBDP[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneBDP[i].sample)))
			#add CNV only
			if GeneD.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneD[i])
				outST=outST+"\t"+str(len(set(GeneD[i].sample)))+"\t"+str(len(set(GeneD[i].cover)))+"\t"+str(len(set(GeneD[i].disrupt)))+"\t"+str(len(set(GeneD[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneD[i].sample)))
			else :
				outST=outST+"\t"+outNull
			#add SV only
			if GeneBP.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneBP[i])
				outST=outST+"\t"+str(len(set(GeneBP[i].sample)))+"\t"+str(len(set(GeneBP[i].cover)))+"\t"+str(len(set(GeneBP[i].disrupt)))+"\t"+str(len(set(GeneBP[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneBP[i].sample)))
			else :
				outST=outST+"\t"+outNull
			#add BrD only
			if GeneB.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneB[i])
				outST=outST+"\t"+str(len(set(GeneB[i].sample)))+"\t"+str(len(set(GeneB[i].cover)))+"\t"+str(len(set(GeneB[i].disrupt)))+"\t"+str(len(set(GeneB[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneB[i].sample)))
			else :
				outST=outST+"\t"+outNull
			#add PI only
			if GeneP.has_key(i) :
				delO, dupO, insO, invO, traO = getOut(GeneP[i])
				outST=outST+"\t"+str(len(set(GeneP[i].sample)))+"\t"+str(len(set(GeneP[i].cover)))+"\t"+str(len(set(GeneP[i].disrupt)))+"\t"+str(len(set(GeneP[i].utr)))+"\t"+delO+"\t"+dupO+"\t"+insO+"\t"+invO+"\t"+traO+"\t"+":".join(list(set(GeneP[i].sample)))
			else :
				outST=outST+"\t"+outNull
			outF.write(outST+"\n")
	outF.close()

main()

	





