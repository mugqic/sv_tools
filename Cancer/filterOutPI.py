#!/usr/bin/python

### Mathieu Bourgey (2011/12/01) - mbourgey@genomequebec.com
### filter out Pindel SV results 


import os
import sys
import string
import getopt
import re


def getarg(argument):
	strd=""
	fil=""
	nor=5
	tum=5
	optli,arg = getopt.getopt(argument[1:],"f:n:t:o:s:h",['file','normal','tumor','output','sample','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			fil=str(value)
		if option in ("-n","--normal"):
			nor=int(value)
		if option in ("-t","--tumor"):
			tum=int(value)
		if option in ("-s","--sample"):
			strd=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	#if not (os.path.exists(fil+"_D") or os.path.exists(fil+"_INV") or os.path.exists(fil+"_LI") or os.path.exists(fil+"_SI") :
                #sys.exit("Error - BreakDancer result file not found:\n"+str(fil))
	if strd > 1 and  strd < 0 :
		sys.exit("Error - Strand filter unknown (0 - 1):\n"+str(strd))
	return fil, nor, tum, strd, out




def usage():
	print "\n---------------------------------------------------------------------------------"
	print "filterOutPI.py filter out Pindel SV results"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : filterOutPI.py [option] "
	print "       -f :        Pindel result file"
	print "       -n :        normal min read support"
	print "       -t :        tumoral min read support"
	print "       -o :        output basename"
	print "       -s :        sample base name"
	print "       -h :        this help \n"

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
 
    return False

def main():
	fil, nor, tum, saN, outb = getarg(sys.argv)
	testType=["DEL","L_INS","S_INS","INV","DUP"]
	outPose=[1,2,0,3,4]
	outType=["small_INDEL","DEL","INS","INV","DUP","TRA"]
	NTcount=[0,0,0,0,0,0]
	Ncount=[0,0,0,0,0,0]
	Tcount=[0,0,0,0,0,0]
	Acount=[0,0,0,0,0,0]
	outF=open(outb+".filteredSV.txt",'w')
	outF.write("Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\n")
	outID=open(outb+".INDEL.txt",'w')
	outID.write("Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\n")
	outS=open(outb+".Summary_count.txt",'w')
	extF=["D","LI","SI","INV","TD"]
	for i in range(0,5,1) :
		print i
		cptL=0
		cptLI=0
		fi=open(fil+"_"+extF[i],'r')
		li=fi.readline()
		while li != "" :
			if li[0] == "#" :
				writeL=False
				keepLine=True
				li=fi.readline()
				ci=li.split()
				if (i != 1) and len(ci) ==  41 :
					cptL=cptL+1
					#print li
					#print len(ci)
					#print ci
					posO=outPose[i]
					for j in range(0,len(ci),1) :
						if ci[j] == "BLOOD" :
							if is_number(ci[j+2]) and is_number(ci[j+4]) :
								Nrs=int(ci[j+2])+int(ci[j+4])
							else : 
								keepLine=False
						elif ci[j] == "TUMOR" :
							if is_number(ci[j+2]) and is_number(ci[j+4]) :
								Trs=int(ci[j+2])+int(ci[j+4])
							else : 
								keepLine=False
						elif ci[j] == "ChrID" :
							chrP=ci[j+1]
						elif ci[j] == "BP" :
							if is_number(ci[j+1]) and is_number(ci[j+2]) :
								cptLI=cptLI+1
								st=ci[j+1]
								en=ci[j+2]
								siz=str(abs(int(en)-int(st)))
							else : 
								keepLine=False
					if keepLine:
						if posO == 1 and int(siz) < 50 :
							posO=0
						typeSV=outType[posO]
						supR=str(Nrs)+"/"+str(Trs)
						if Trs >= tum  and Nrs == 0:
							Tcount[posO]=Tcount[posO]+1
							state="T"
							writeL=True
						elif Trs >= tum  and Nrs >= nor:
							NTcount[posO]=NTcount[posO]+1
							state="NT"
							writeL=True
						elif Trs == 0 and Nrs >= nor:
							Ncount[posO]=Ncount[posO]+1
							state="N"
							writeL=True
						elif Trs >= tum or Nrs >= nor:
							Acount[posO]=Acount[posO]+1
							state="A"
							writeL=True
						if writeL :
							if typeSV != "small_INDEL" :
								outF.write("PI\t"+saN+"\t"+chrP+"\t"+st+"\t"+en+"\t"+chrP+"\t"+siz+"\t"+typeSV+"\t"+supR+"\t"+state+"\n")
							else :
								outID.write("PI\t"+saN+"\t"+chrP+"\t"+st+"\t"+en+"\t"+chrP+"\t"+siz+"\t"+typeSV+"\t"+supR+"\t"+state+"\n")
				elif i == 1 and len(ci) ==  20:
					cptL=cptL+1
					#print li
					#print len(ci)
					#print ci
					posO=outPose[i]
					for j in range(0,len(ci),1) :
						if ci[j] == "BLOOD" :
							if is_number(ci[j+2]) and is_number(ci[j+4]) :
								Nrs=int(ci[j+2])+int(ci[j+4])
							else : 
								keepLine=False
						elif ci[j] == "TUMOR" :
							if is_number(ci[j+2]) and is_number(ci[j+4]) :
								Trs=int(ci[j+2])+int(ci[j+4])
							else : 
								keepLine=False
						elif ci[j] == "ChrID" :
							chrP=ci[j+1]
					if is_number(ci[5]) and is_number(ci[8]) :
						cptLI=cptLI+1
						st=ci[5]
						en=ci[8]
						siz=str(abs(int(en)-int(st)))
					else : 
						keepLine=False
					if keepLine:
						if posO == 1 and int(siz) < 50 :
							posO=0
						typeSV=outType[posO]
						supR=str(Nrs)+"/"+str(Trs)
						if Trs >= tum  and Nrs == 0:
							Tcount[posO]=Tcount[posO]+1
							state="T"
							writeL=True
						elif Trs >= tum  and Nrs >= nor:
							NTcount[posO]=NTcount[posO]+1
							state="NT"
							writeL=True
						elif Trs == 0 and Nrs >= nor:
							Ncount[posO]=Ncount[posO]+1
							state="N"
							writeL=True
						elif Trs >= tum or Nrs >= nor:
							Acount[posO]=Acount[posO]+1
							state="A"
							writeL=True
						if writeL :
							if typeSV != "small_INDEL" :
								outF.write("PI\t"+saN+"\t"+chrP+"\t"+st+"\t"+en+"\t"+chrP+"\t"+siz+"\t"+typeSV+"\t"+supR+"\t"+state+"\n")
							else :
								outID.write("PI\t"+saN+"\t"+chrP+"\t"+st+"\t"+en+"\t"+chrP+"\t"+siz+"\t"+typeSV+"\t"+supR+"\t"+state+"\n")
					
			li=fi.readline()
		fi.close()
		print cptL
		print cptLI
	outF.close()
	outS.write("Total\tINDEL\tDEL\tINS\tINV\tDUP\tTRA\n")
	outS.write(str(sum(Tcount))+"\t"+str(Tcount[0])+"\t"+str(Tcount[1])+"\t"+str(Tcount[2])+"\t"+str(Tcount[3])+"\t"+str(Tcount[4])+"\t"+str(Tcount[5])+"\n")
	outS.write(str(sum(NTcount))+"\t"+str(NTcount[0])+"\t"+str(NTcount[1])+"\t"+str(NTcount[2])+"\t"+str(NTcount[3])+"\t"+str(NTcount[4])+"\t"+str(NTcount[5])+"\n")
	outS.write(str(sum(Ncount))+"\t"+str(Ncount[0])+"\t"+str(Ncount[1])+"\t"+str(Ncount[2])+"\t"+str(Ncount[3])+"\t"+str(Ncount[4])+"\t"+str(Ncount[5])+"\n")
	outS.write(str(sum(Acount))+"\t"+str(Acount[0])+"\t"+str(Acount[1])+"\t"+str(Acount[2])+"\t"+str(Acount[3])+"\t"+str(Acount[4])+"\t"+str(Acount[5])+"\n")
	outS.close()

main()

	    
	
	





