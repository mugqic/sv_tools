#!/bin/sh
# 
##2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com
##This pipeline runs the detection of indel and structural variation on paired normal tumoral Whole genome sequence data
##need this bash located in the same directory than this script:
#	jobTestMoab.sh
#	mergeBrD.sh
#	RunDNAC.5.R
#	runPindel.sh
#	mergePindelChro.sh
#	filterOutBrD.py 
#	filterOutPI.py 
#	filterOutDNAC.py
#	filterBedResults.sh
#	rtxt2rbed.sh
#	CollapseSVC.py
#	

echo "-----------------------------------------------------------------------------"
echo "runSVcancer.sh: 2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com"
echo "This pipeline runs the detection of indel and structural variation "
echo "on paired normal/tumoral whole genome illumina hiseq sequence data"
echo "-----------------------------------------------------------------------------"

TUM_FILE=""
MET_TUM_FILE=""
NOR_FILE=""
MET_NOR_FILE=""
BIN_SIZE=""
REF_DIR=""
OUTPUT_BASENAME=""
START_STEP="0"
LAST_STEP="0"
EXCL_MAP=""
GENE_CORD=""
EMAIL_ADD=""

function usage {

            
            echo "Usage: `basename $0` [option] <value>"
            echo ""
            echo "runSVcancer2.0.sh - launches the pipeline to detect Structural Variations in Whole genome paired Normal/Tumoral data"
            echo "Uses MOAB msub [required]"
            echo ""
            echo "Options:"
            echo "-t                    tumor bam file"
            echo "-m                    tumor insert size metrics file"
            echo "-T                    tumor mean coverage"
            echo "-n                    normal bam file"
            echo "-l                    normal insert size metrics file"
            echo "-N                    normal mean coverage"
            echo "-b                    bin size"
            echo "-o                    output basename"
            echo "-r                    reference genome folder"
            echo "-x                    exclusion bed file"
            echo "-i                    inclusion bed file"
            echo "-g                    ccds gene coordinate bed file"
            echo "-d                    DGV bed file"
            echo "-a                    microsatelite bed file"
            echo "-c                    repeatMasker bed file" 
            echo "-E                    email adress to receive moab e-mail" 
            echo "-v                    version"
            echo "-s                    start step"
            echo "-e                    end step"
            echo "-h                    this help"
            echo "Steps:"
	    echo "			1: BreakDancer"
            echo "			2: DNAC"
	    echo "			3: Pindel"
	    echo "			4: filter results"
	    echo "			5: Bed tracks"

exit 0

}

while getopts “h:t:m:T:n:l:N:b:s:e:x:i:g:d:a:c:E:o:r:v” OPT
do
    case "$OPT" in
        h)
            usage
            exit 0
            ;;
        v)
            echo "`basename $0` version 1.2"
            exit 0
            ;;
        t) 
           TUM_FILE=$OPTARG
           ;;
        m)
           MET_TUM_FILE=$OPTARG
           ;;
        T)
           COVM_TUM=$OPTARG
           ;;
        n) 
           NOR_FILE=$OPTARG
           ;;
        l)
           MET_NOR_FILE=$OPTARG
           ;;
        N)
           COVM_NOR=$OPTARG
           ;;
        b)
           BIN_SIZE=$OPTARG
           ;;
        o)
           OUTPUT_BASENAME=$OPTARG
           ;;
        r)
           REF_DIR=$OPTARG
           ;;
        x)
           EXCL_MAP=$OPTARG
           ;;
        i)
           INCL_MAP=$OPTARG
           ;;
        g)
           GENE_CORD=$OPTARG
           ;;
        d)
           DGV_CORD=$OPTARG
           ;;
        a)
           MICRO_SAT_CORD=$OPTARG
           ;;
        c)
           REPEATM_CORD=$OPTARG
           ;;
        E)
           EMAIL_ADD=$OPTARG
           ;;
        s) 
           START_STEP=$OPTARG
           ;;
        e)
           LAST_STEP=$OPTARG
           ;;
        
    esac

done


        if [[ -z $TUM_FILE ]] || [[ -z $MET_TUM_FILE ]] || [[ -z $NOR_FILE ]] || [[ -z $MET_NOR_FILE ]] || [[ -z $BIN_SIZE ]] || [[ -z $REF_DIR ]] || [[ -z $OUTPUT_BASENAME ]]  
then
     echo "Given argument tumor file:  $TUM_FILE\ntumor metrics file:  $MET_TUM_FILE\nnormal file: $NOR_FILE  \nnormal metrics file: $MET_NOR_FILE \nbin size: $BIN_SIZE  \nref: $REF_DIR  \nout: $OUTPUT_BASENAME \nexculsion map file: $EXCL_MAP \ngene coordinate file: $GENE_CORD \n"
     usage
     exit 1
fi


# Start function 0

function function_0 {
     usage
     exit 1
}

##get general info

execPath=$(dirname $0)
workDir=$(dirname $OUTPUT_BASENAME)
baseName=$(basename $OUTPUT_BASENAME)
pidNum=$$

echo $pidNum

#Look for files

# if [ ! -f $REF_FASTA ];then 
#  echo "reference fasta file $REF_FASTA not found."; exit 1;
# fi
if [ ! -d $workDir/ ]; then
        mkdir $workDir
fi

cd $workDir

if [ ! -d $OUTPUT_DIR/ ]; then
        mkdir $OUTPUT_DIR
fi

if [ ! -d jobs_output/ ]; then
        mkdir jobs_output/
fi

if [ ! -d command_logs ]; then
        mkdir command_logs
fi

###env variables setting
SAMTOOLS_HOME="/lb/project/mugqic/software/tools/samtools/"
BED_TOOLS_HOME="/lb/project/mugqic/software/tools/BEDTools-Version-2.15.0/bin/"
GATK_JAR="/lb/project/mugqic/software/tools/GenomeAnalysisTK/GenomeAnalysisTK.jar"
JOB_OUT=$workDir/jobs_output/
CMD_LOG=$workDir/command_logs

TinS=$(grep -A 1 "MEDIAN"  $MET_TUM_FILE | awk ' NR == 2 {print $1} ') 
NinS=$(grep -A 1 "MEDIAN"  $MET_NOR_FILE | awk ' NR == 2 {print $1} ') 

TinSD=$(grep -A 1 "MEDIAN"  $MET_TUM_FILE | awk ' NR == 2 {print $6} ') 
NinSD=$(grep -A 1 "MEDIAN"  $MET_NOR_FILE | awk ' NR == 2 {print $6} ') 
wait

echo "JOBs informations are provided in : runSVc.log.$pidNum"


##---------------
##PEM analysis
##---------------
function function_1 {


echo "step 1 - Running PEM analysis : BreakDancer"

logfile="$CMD_LOG/BreakDancer.log"

if [ -f $logfile ]; then
        rm $logfile
	rm $JOB_OUT/breakDT_conf.out
	rm $JOB_OUT/breakD_conf.out
	rm $JOB_OUT/breakDCF.out
	rm $JOB_OUT/breakD_TR.out
	rm $JOB_OUT/formatBrD.out 
	rm $JOB_OUT/mergeBrD.out
	rm $JOB_OUT/mergeBrD.out 
	rm $JOB_OUT/breakD.clean.out 
fi

echo "workdir : "${workDir}

rm $OUTPUT_BASENAME.breakD.*  $JOB_OUT/breakD_*

# make 3SD in T vs 2 in N
string="/lb/project/mugqic/software/SV_indel/breakdancer-1.1_2011_02_21/perl/bam2cfg.pl -g -h -c 3 $TUM_FILE > $OUTPUT_BASENAME.breakDT.cfg"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakDT_conf.out -m ae -M ${EMAIL_ADD} -N BrDaT_conf.$pidNum -V -l walltime=30:00:00 > $CMD_LOG/runSVc.log.$pidNum
 
string="/lb/project/mugqic/software/SV_indel/breakdancer-1.1_2011_02_21/perl/bam2cfg.pl -g -h -c 2  $NOR_FILE > $OUTPUT_BASENAME.breakDN.cfg"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakDN_conf.out -m ae -M ${EMAIL_ADD} -N BrDaN_conf.$pidNum -V -l walltime=30:00:00 > $CMD_LOG/runSVc.log.$pidNum
 
 
string="cat $OUTPUT_BASENAME.breakDN.cfg $OUTPUT_BASENAME.breakDT.cfg > $OUTPUT_BASENAME.breakD.cfg"
echo $string >> $logfile
echo $string
echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/breakDCF.out -m ae -M ${EMAIL_ADD} -N breakDCF.$pidNum -V -l nodes=1:ppn=5 -W x=depend:afterok:BrDaN_conf.$pidNum:BrDaT_conf.$pidNum > $CMD_LOG/runSVc.log.$pidNum
 
string="breakDancer -t -c 3 -b 500 -r 2 -s 7 -m 1000000 -g $OUTPUT_BASENAME.breakD.TR.bed -d $OUTPUT_BASENAME.breakD.TR.ctx $OUTPUT_BASENAME.breakD.cfg > $OUTPUT_BASENAME.breakD.TR.ctx"
echo $string >> $logfile
echo $string
echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/breakD_TR.out -m ae -M ${EMAIL_ADD} -N BrDa.$pidNum -V -l walltime=100:00:00,nodes=1:ppn=12 -W x=depend:afterok:breakDCF.$pidNum > $CMD_LOG/runSVc.log.$pidNum


if [ ! -d  ${workDir}/tmpbam ]; then
	mkdir ${workDir}/tmpbam
fi

samtools view -H $NOR_FILE | grep "@SQ" | awk ' {print $2} ' | cut -d: -f 2 | grep -v "GL" | grep -v "MT" >  ${workDir}/tmpbam/tmpchr.txt

for i in  `cat  ${workDir}/tmpbam/tmpchr.txt `
do
rm  $JOB_OUT/breakD_stT_$i.out
rm $JOB_OUT/breakD_stN_$i.out
rm $JOB_OUT/breakD_conf.$i.out
rm $JOB_OUT/breakD_$i.out

#if no chrom bam:
string="samtools view -h -b $TUM_FILE $i > ${workDir}/tmpbam/T.$i.bam "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_stT_$i.out -N spT.$i.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=5:00:00 > $CMD_LOG/runSVc.log.$pidNum

string="samtools view -h -b $NOR_FILE $i > ${workDir}/tmpbam/N.$i.bam "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_stN_$i.out -N spN.$i.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=5:00:00 > $CMD_LOG/runSVc.log.$pidNum


string="sed \"s|$TUM_FILE|${workDir}/tmpbam/T.$i.bam|g\" $OUTPUT_BASENAME.breakD.cfg | sed \"s|$NOR_FILE|${workDir}/tmpbam/N.$i.bam|g\" > $OUTPUT_BASENAME.breakD.$i.cfg"
 echo $string >> $logfile
 echo $string
 echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_conf.$i.out -N BrDa_conf.$i.$pidNum -V -m ae -M ${EMAIL_ADD} -W x=depend:afterok:breakDCF.$pidNum > $CMD_LOG/runSVc.log.$pidNum

string="breakDancer -c 3 -r 2 -m 1000000 -g $OUTPUT_BASENAME.breakD.$i.bed -d $OUTPUT_BASENAME.breakD.$i.ctx $OUTPUT_BASENAME.breakD.$i.cfg > $OUTPUT_BASENAME.breakD.$i.ctx "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_$i.out -N BrDa.$i.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=10:00:00,nodes=1:ppn=12 -W x=depend:afterok:BrDa_conf.$i.$pidNum:spN.$i.$pidNum:spT.$i.$pidNum> $CMD_LOG/runSVc.log.$pidNum

string="sed \"s|${workDir}/tmpbam/T.$i.bam|$TUM_FILE|g\" $OUTPUT_BASENAME.breakD.$i.ctx | sed \"s|${workDir}/tmpbam/N.$i.bam|$NOR_FILE|g\" > $OUTPUT_BASENAME.breakD.$i.ctx.tmp && mv $OUTPUT_BASENAME.breakD.$i.ctx.tmp $OUTPUT_BASENAME.breakD.$i.ctx"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/formatBrD.out -N BrDa_format.$pidNum -V -m ae -M ${EMAIL_ADD} -W x=depend:afterok:BrDa.$i.$pidNum  > $CMD_LOG/runSVc.log.$pidNum

done


string="$execPath/mergeBrD.sh ${workDir} $OUTPUT_BASENAME"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/mergeBrD.out -N mergeBrD.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=10:00:00 -W x=depend:afterok:BrDa_format.$pidNum > $CMD_LOG/runSVc.log.$pidNum


string="rm -rf $OUTPUT_BASENAME*.fastq " 
echo $string >> $logfile
echo $string
echo $string| msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD.clean.out -N BrDa.clean.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=3:00:00 -W x=depend:afterok:mergeBrD.$pidNum >> $CMD_LOG/runSVc.log.$pidNum

}


##---------------
##RD analysis
##---------------
# Start function 2

function function_2 {
 
echo "step 2 - Running RD analysis :DNAc"

logfile="$CMD_LOG/CNVnator.log"

if [ -f $logfile ]; then
       rm $logfile
fi

#string="java -Xmx15g -jar  $GATK_JAR -T CountReads -R /lb/project/mugqic/genomes/human_g1k_v37/human_g1k_v37.fasta -L /lb/project/mugqic/genomes/human_g1k_v37/tmp_genome1kbBin.bed -I D000G2K/D000G2K.sorted.dup.bam

#DNAC
string="Rscript_2.15 $execPath/RunDNAC.5.R -n $NOR_FILE -t $TUM_FILE -o $OUTPUT_BASENAME.DNAC_10000"
echo $string >> $logfile
echo $string
echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/DNACfilt.1.out -M ${EMAIL_ADD} -N DNACfilt.1.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=24:00:00,nodes=1:ppn=12 >>  $CMD_LOG/runSVc.log.$pidNum


for i in  `seq 1 1 22 `
do
	string="Rscript_2.15 $execPath/RunDNAC.5.R -n $NOR_FILE -t $TUM_FILE -o $OUTPUT_BASENAME.DNAC_500 -b 500 -c $i"
	echo $string >> $logfile
	echo $string
	echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/DNACfilt.2.$i.out -M ${EMAIL_ADD} -N DNACfilt.2.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=10:00:00,nodes=1:ppn=6 >>  $CMD_LOG/runSVc.log.$pidNum

	string="Rscript_2.15 $execPath/RunDNAC.5.R -n $NOR_FILE -t $TUM_FILE -o $OUTPUT_BASENAME.DNAC_100 -b 100 -c $i"
	echo $string >> $logfile
	echo $string
	echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/DNACfilt.3.$i.out -M ${EMAIL_ADD} -N DNACfilt.3.$pidNum -V -m ae -M ${EMAIL_ADD} -l walltime=10:00:00,nodes=1:ppn=12 >>  $CMD_LOG/runSVc.log.$pidNum
done

touch $OUTPUT_BASENAME.DNAC_500.txt $OUTPUT_BASENAME.DNAC_100.txt
string="for i in  `seq 1 1 22 ` ; do cat $OUTPUT_BASENAME.DNAC_500.$i.txt >> $OUTPUT_BASENAME.DNAC_500.txt ; cat $OUTPUT_BASENAME.DNAC_100.$i.txt >> $OUTPUT_BASENAME.DNAC_100.txt ; done"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/mergeDNACfilt.out -M ${EMAIL_ADD} -N mergeDNACfilt.2_3.$pidNum -V -m ae -M ${EMAIL_ADD} -l nodes=1:ppn=1 -W x=depend:afterok:DNACfilt.2.$pidNum:DNACfilt.3.$pidNum  >>  $CMD_LOG/runSVc.log.$pidNum


}
##---------------
##SR analysis
##---------------

# Start function 3

function function_3 {
 
echo "step 3 - Running SR analysis : Pindel"

logfile="$CMD_LOG/Pindel.log"

if [ -f $logfile ]; then
        rm $logfile
fi


## generate normal-tumoral pair configuration file
######must be out-sourced in a specified bash
printf "$TUM_FILE\t$TinS\tTUMOR\n$NOR_FILE\t$NinS\tBLOOD\n" > $OUTPUT_BASENAME.Pindel.conf
wait

TESTD=$($execPath/jobTestMoab.sh mergeBrD.$pidNum)
##check dependency on breakDancer
if [ $TESTD == 0 ]; then
	L_LIST="walltime=52:00:00,nodes=1:ppn=12 -W x=depend:afterok:mergeBrD.$pidNum"
else
	L_LIST="walltime=52:00:00,nodes=1:ppn=12"
	if [ ! -d  ${workDir}/tmpbam ]; then
		mkdir ${workDir}/tmpbam
	fi
	samtools view -H $NOR_FILE | grep "@SQ" | awk ' {print $2} ' | cut -d: -f 2 | grep -v "GL" >  ${workDir}/tmpbam/tmpchr.txt
fi
##Pindel
#run Pindel chro by chro
mergeDep=""


for i in  `cat  ${workDir}/tmpbam/tmpchr.txt `
do
string="$execPath/runPindel.sh $REF_DIR $i $OUTPUT_BASENAME $OUTPUT_BASENAME.breakD.ctx " 
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/PI.chr$i.out -N PI.chr$i.$pidNum -V -m ae -M ${EMAIL_ADD} -l $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
mergeDep=$mergeDep:PI.chr$i.$pidNum 
done


#merge results, err and out files
echo "$execPath/mergePindelChro.sh $OUTPUT_BASENAME $JOB_OUT" | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/mergePI.out -m ae -M ${EMAIL_ADD} -N mergePI.$pidNum -V -m ae -M ${EMAIL_ADD} -W x=depend:afterok:$mergeDep >> $CMD_LOG/runSVc.log.$pidNum

}



##filterOut
# Start function 4

function function_4 {
 
echo "step 4 - filtering out"

logfile="$CMD_LOG/filterOut.log"

if [ -f $logfile ]; then
        rm $logfile
fi
TESTD=$($execPath/jobTestMoab.sh mergePI.$pidNum)
##check dependency on PI wich depends on breakdancer
if [ $TESTD == 0 ]; then
	L_LIST="-W x=depend:afterok:mergePI.$pidNum:DNACfilt.1.$pidNum:mergeDNACfilt.2_3.$pidNum"
else
	L_LIST=""
fi
L_LIST=""

string=" $execPath/filterOutBrD.py -f $OUTPUT_BASENAME.breakD.ctx -n 10 -b $NOR_FILE -c $TUM_FILE -t 10 -o $OUTPUT_BASENAME.BrD -s $baseName "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterBD.out -m ae -M ${EMAIL_ADD} -N filterBD.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.BrD.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.BrD BD"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterBrD2.out -m ae -M ${EMAIL_ADD} -N filterBrD2.$pidNum -V -m ae -M ${EMAIL_ADD} -W x=depend:afterok:filterBD.$pidNum >> $CMD_LOG/runSVc.log.$pidNum
# 
string=" $execPath/filterOutPI.py -f $OUTPUT_BASENAME.Pindel -n 10 -t 10 -o $OUTPUT_BASENAME.PI -s $baseName "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterPI.out -m ae -M ${EMAIL_ADD} -N filterPI.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST  >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.PI.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.PI PI"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterPI2.out -m ae -M ${EMAIL_ADD} -N filterPI2.$pidNum -V -W x=depend:afterok:filterPI.$pidNum >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterOutDNAC.sh $OUTPUT_BASENAME.DNAC_10000.txt $OUTPUT_BASENAME.DNAC_10000.filteredSV.txt $baseName 5"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC.1.out -m ae -M ${EMAIL_ADD} -N filterDNAC.1.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterOutDNAC.sh $OUTPUT_BASENAME.DNAC_500.txt $OUTPUT_BASENAME.DNAC_500.filteredSV.txt $baseName 5"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC.2.out -m ae -M ${EMAIL_ADD} -N filterDNAC.2.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterOutDNAC.sh $OUTPUT_BASENAME.DNAC_100.txt $OUTPUT_BASENAME.DNAC_100.filteredSV.txt $baseName 5"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC.3.out -m ae -M ${EMAIL_ADD} -N filterDNAC.3.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.DNAC_10000.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.DNAC_10000 DNAC_10000 1"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC2.1.out -m ae -M ${EMAIL_ADD} -N filterDNAC2.1.$pidNum -V -W x=depend:afterok:filterDNAC.1.$pidNum >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.DNAC_500.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.DNAC_500 DNAC_500 1"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC2.2.out -m ae -M ${EMAIL_ADD} -N filterDNAC2.2.$pidNum -V -W x=depend:afterok:filterDNAC.2.$pidNum >> $CMD_LOG/runSVc.log.$pidNum

string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.DNAC_100.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.DNAC_100 DNAC_100 1"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC2.3.out -m ae -M ${EMAIL_ADD} -N filterDNAC2.3.$pidNum -V -W x=depend:afterok:filterDNAC.3.$pidNum >> $CMD_LOG/runSVc.log.$pidNum

}

##generate Bed tracks and annotate
# Start function 5

function function_5 {
 
echo "step 5 - Generate BED TRACK and annotate"

TESTD=$($execPath/jobTestMoab.sh filterPI2.$pidNum)
##check dependency on breakDancer
if [ $TESTD == 0 ]; then
	L_LIST="-W x=depend:afterok:filterBrD2.$pidNum:filterPI2.$pidNum:filterPI2.$pidNum:filterDNAC2.1.$pidNum:filterDNAC2.2.$pidNum"
else
	L_LIST=""
fi

logfile="$CMD_LOG/bedAnnot.log"

if [ -f $logfile ]; then
        rm $logfile
fi

##generate individuals germline tracks
string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.BrD.other.filteredSV.annotate.txt $OUTPUT_BASENAME.BrD.other.filteredSV.annotate.bed "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDBrD.out -m ae -M ${EMAIL_ADD} -N bedBrD.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.PI.other.filteredSV.annotate.txt $OUTPUT_BASENAME.PI.other.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDPI.out -m ae -M ${EMAIL_ADD} -N bedPI.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC_10000.other.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC_10000.other.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC.1.out -m ae -M ${EMAIL_ADD} -N bedDNAC.1.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum


string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC_500.other.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC_500.other.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC.2.out -m ae -M ${EMAIL_ADD} -N bedDNAC.2.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum


string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC_100.other.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC_100.other.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC.3.out -m ae -M ${EMAIL_ADD} -N bedDNAC.3.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

##generate individuals tumSpe tracks
string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.BrD.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.BrD.TumS.filteredSV.annotate.bed "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDBrD.out -m ae -M ${EMAIL_ADD} -N bedBrD.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.PI.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.PI.TumS.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDPI.out -m ae -M ${EMAIL_ADD} -N bedPI.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC_10000.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC_10000.TumS.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC2.1.out -m ae -M ${EMAIL_ADD} -N bedDNAC.1.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC_500.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC_500.TumS.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC2.2.out -m ae -M ${EMAIL_ADD} -N bedDNAC.2.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum

string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC_100.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC_100.TumS.filteredSV.annotate.bed  "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC2.3.out -m ae -M ${EMAIL_ADD} -N bedDNAC.3.$pidNum -V -m ae -M ${EMAIL_ADD} $L_LIST >> $CMD_LOG/runSVc.log.$pidNum



}



# Execute functions from $START_STEP to $LAST_STEP

for step in `seq $START_STEP $LAST_STEP`
do
function_$step
done

