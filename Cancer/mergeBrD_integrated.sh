#!/bin/sh
# 

#Usage: mergeBrD.sh Workdir($1) output_basename($2) 

FIRST="0"
for i in  `cat  $1/chromosomes.txt | grep -v "GL" | grep -v "MT"`
do
if [ $FIRST == "0" ] ; then
	cat $2.breakD.$i.ctx >>  $2.breakD.ctx
  FIRST=1;
else
	grep -v "^#" $2.breakD.$i.ctx >>  $2.breakD.ctx
fi
cat $2.breakD.$i.bed >>  $2.breakD.bed

done

grep -v "^#" $2.breakD.TR.ctx >>  $2.breakD.ctx
cat $2.breakD.TR.bed >>  $2.breakD.bed

