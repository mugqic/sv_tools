#!/bin/sh

#Usage filterOutDNAC.sh ftout_input($1) output($2) sample($3) bin_number($4)

#get param
INPUT_FILE=$1
OUTPUT_FILE=$2
NAME=$3
BIN_NUM=$4

##withambigous
awk -v nam=$NAME -v sizB=$BIN_NUM ' BEGIN {
	print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tCopyNum_N/T\tOnco_State"
} {
	if ($2 != "X" || $2 != "Y") {
		if ($5 >= sizB) {
			onco="T"
			si=$4-$3
			if ($8 >= 2.5) {
				typ="DUP"
				levelD=$8
# 				if (onco == "T") {
# 					typ="DUP"
# 					levelD=$8
# 				} else {
# 					onco="T"
# 					typ="DEL"
# 					levelD=$8 "N"
# 				}
				print "DNAcopy\t" nam "\t" $2 "\t" $3 "\t" $4 "\t" $2 "\t" si "\t" typ "\t" $5 "_" $6   "_" levelD "\t" onco
				
			} else if ($8 <= 1.5) {
				typ="DEL"
				levelD=$8
# 				if (onco == "T") {
# 					typ="DEL"
# 					levelD=$8
# 				} else {
# 					onco="T"
# 					typ="DUP"
# 					levelD=$8 "N"
# 				}
				print "DNAcopy\t" nam "\t" $2 "\t" $3 "\t" $4 "\t" $2 "\t" si "\t" typ "\t" $5 "_" $6   "_" levelD "\t" onco
			} 
		}
	}
} ' $INPUT_FILE > $OUTPUT_FILE

##without ambigous 
# awk -v nam=$NAME -v sizB=$BIN_NUM ' BEGIN {
# 	print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tCopyNum"
# } {
# 	if ($2 != "X" || $2 != "Y") {
# 		if ($5 >= sizB) {
# 			si=$4-$3
# 			if ($7 >= 3) {
# 				typ="DUP"
# 				print "DNAcopy\t" nam "\t" $2 "\t" $3 "\t" $4 "\t" $2 "\t" si "\t" typ "\t" $5 "_" $6   "_" $7
# 				
# 			} else if ($7 <= 1) {
# 				typ="DEL"
# 				print "DNAcopy\t" nam "\t" $2 "\t" $3 "\t" $4 "\t" $2 "\t" si "\t" typ "\t" $5 "_" $6   "_" $7 
# 			}
# 		}
# 	}
# } ' $INPUT_FILE > $OUTPUT_FILE