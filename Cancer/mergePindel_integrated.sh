#!/bin/sh
# 
##2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com

##Usage: mergePindelChro.sh basenamePindelOutput($1) workdir($2)

rm -f ${1}_BP
rm -f ${1}_D
rm -f ${1}_INV
rm -f ${1}_LI
rm -f ${1}_SI
rm -f ${1}_TD

touch ${1}_BP
touch ${1}_D
touch ${1}_INV
touch ${1}_LI
touch ${1}_SI
touch ${1}_TD
for i in `cat  $2/chromosomes.txt | grep -v "GL" | grep -v "MT"`
do
cat $1.${i}_BP >> ${1}_BP
cat $1.${i}_D >> ${1}_D
cat $1.${i}_INV >> ${1}_INV
cat $1.${i}_LI >> ${1}_LI
cat $1.${i}_SI >> ${1}_SI
cat $1.${i}_TD >> ${1}_TD
done

