#!/bin/sh

#2012/05/04 - Mathieu Bourgey - mbourgey@genomequebec.com
#format res .txt to res .bed file
#color code :
#DEL: BLACK	=> 0,0,0
#INS: RED	=> 220,0,0
#INV: GREEN	=> 0,220,0
#DUP: BLUE	=> 0,0,220
#TRA: YELLOW	=> 220,220,0


#Usage: rtxt2rbed.sh txt_file($1) bed_file($2)

#parm
T_FILE=$1
B_FILE=$2
HEAD="track name=\"${T_FILE}\" description=\"SV calls\" visibility=2"

#main
echo $HEAD > $B_FILE
awk ' BEGIN {
	del="0,0,0"
	ins="220,0,0"
	inv="0,220,0"
	dup="0,0,220"
	tra="220,220,0"
}
NR > 1 {
	if ($3 == "X") {
		$3=23
	} else if ($3 == "Y") {
		$3=24
	}
	if ($8 == "DEL") {
		outC=del
		dou=0
	} else if ($8 == "INV") {
		outC=inv
		dou=0
	} else if ($8 == "INS") {
		outC=ins
		dou=0
	} else if ($8 == "DUP") {
		outC=dup
		dou=0
	} else {
		outC=tra
		dou=1
	}
	if (dou == 0) {
		print $3 "\t" $4 "\t" $5 "\t" $10  "\t0\t+\t" $4 "\t" $5 "\t" outC
	} else {
		print $3 "\t" $4 "\t" $4+1 "\t" $10 "_" $6 "_" $5 "\t0\t+\t" $4 "\t" $4+1 "\t" outC
		print $6 "\t" $5-1 "\t" $5 "\t" $10 "_" $3 "_" $4 "\t0\t+\t" $5-1 "\t" $5 "\t" outC
	}
} ' ${T_FILE} | sort -k1,1n -k2,2n | awk ' {print "chr" $0} ' | sed "s/chr23/chrX/g" | sed "s/chr24/chrY/g" >> $B_FILE
