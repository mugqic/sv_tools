#!/usr/bin/python

### Mathieu Bourgey (2011/12/01) - mbourgey@genomequebec.com
### filter out Pindel SV results 


import os
import sys
import string
import getopt
import re


def getarg(argument):
	strd=""
	fil=""
	nor=5
	tum=5
	optli,arg = getopt.getopt(argument[1:],"f:n:t:o:s:h",['file','normal','output','sample','help'])
	if len(optli) == 0 :
		usage()
		sys.exit("Error : No argument given")
	for option, value in optli:
		if option in ("-f","--file"):
			fil=str(value)
		if option in ("-n","--normal"):
			nor=int(value)
		if option in ("-s","--strand"):
			strd=str(value)
		if option in ("-o","--output"):
			out=str(value)
		if option in ("-h","--help"):
			usage()
			sys.exit()
	#if not (os.path.exists(fil+"_D") or os.path.exists(fil+"_INV") or os.path.exists(fil+"_LI") or os.path.exists(fil+"_SI") :
                #sys.exit("Error - BreakDancer result file not found:\n"+str(fil))
	if strd > 1 and  strd < 0 :
		sys.exit("Error - Strand filter unknown (0 - 1):\n"+str(strd))
	return fil, nor, strd, out




def usage():
	print "\n---------------------------------------------------------------------------------"
	print "filterOutPI.py filter out Pindel SV results"
	print "This program was written by Mathieu BOURGEY"
	print "For more information, contact: mbourgey@genomequebec.com"
	print "----------------------------------------------------------------------------------\n"
	print "USAGE : filterOutPI.py [option] "
	print "       -f :        Pindel result file"
	print "       -n :        sample min read support"
	print "       -o :        output basename"
	print "       -s :        sample base name"
	print "       -h :        this help \n"


def main():
	fil, nor, saN, outb = getarg(sys.argv)
	testType=["DEL","L_INS","S_INS","INV","DUP"]
	outPose=[1,2,0,3,4]
	outType=["small_INDEL","DEL","INS","INV","DUP","TRA"]
	NTcount=[0,0,0,0,0,0]
	Ncount=[0,0,0,0,0,0]
	Tcount=[0,0,0,0,0,0]
	Acount=[0,0,0,0,0,0]
	outF=open(outb+".filteredSV.txt",'w')
	outF.write("Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup\n")
	outID=open(outb+".INDEL.txt",'w')
	outID.write("Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup\n")
	outS=open(outb+".Summary_count.txt",'w')
	extF=["D","LI","SI","INV","TD"]
	for i in range(0,5,1) :
		fi=open(fil+"_"+extF[i],'r')
		li=fi.readline()
		while li != "" :
			if li[0] == "#" :
				writeL=False
				li=fi.readline()
				ci=li.split()
				posO=outPose[i]
				for j in range(0,len(ci),1) :
					if ci[j] == "SAMPLE" :
						Nrs=int(ci[j+2])+int(ci[j+4])
					elif ci[j] == "ChrID" :
						chrP=ci[j+1]
					elif ci[j] == "BP" :
						st=ci[j+1]
						en=ci[j+2]
						siz=str(abs(int(en)-int(st)))
					elif ci[j] == "LI" :
						st=ci[j+3]
						en=ci[j+6]
						siz=str(abs(int(en)-int(st)))
				if i < 3 and int(siz) < 50 :
					posO=0
				typeSV=outType[posO]
				supR=str(Nrs)
				if Nrs >= nor:
					Ncount[posO]=Ncount[posO]+1
					writeL=True
				if writeL :
					if typeSV != "small_INDEL" :
						outF.write("PI\t"+saN+"\t"+chrP+"\t"+st+"\t"+en+"\t"+chrP+"\t"+siz+"\t"+typeSV+"\t"+supR+"\n")
					else :
						outID.write("PI\t"+saN+"\t"+chrP+"\t"+st+"\t"+en+"\t"+chrP+"\t"+siz+"\t"+typeSV+"\t"+supR+"\n")
			li=fi.readline()
		fi.close()
	outF.close()
	outS.write("Total\tINDEL\tDEL\tINS\tINV\tDUP\tTRA\n")
	outS.write(str(sum(Ncount))+"\t"+str(Ncount[0])+"\t"+str(Ncount[1])+"\t"+str(Ncount[2])+"\t"+str(Ncount[3])+"\t"+str(Ncount[4])+"\t"+str(Ncount[5])+"\n")
	outS.close()

main()

	    
	
	





