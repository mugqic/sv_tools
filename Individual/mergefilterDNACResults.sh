#!/bin/sh

#Usage mergefilterBedResults.sh ftout_inputLARGE($1) ftout_inputMEDIUM($2) ftout_inputSMALL($3) output($4) 

#parms
BEDTOOL="/lb/project/mugqic/software/tools/BEDTools-Version-2.15.0/bin"
execPath=$(dirname $0)
outDir=$(dirname $4)
echo $workDir
INPUT_FILE_L=$1
INPUT_FILE_M=$2
INPUT_FILE_S=$3
OUTPUT_FILE=$4

#cleaning

for i in "DEL" "DUP" 
do
#cleaning
##create result file by type of events
echo "Generate result files for $i ..."
awk -v ty=$i '
BEGIN {
	comp=0
} 
{
    if ($8 == ty) {
        if ($3 == "X") {
                $3=23
        } 
        else if ($3 =="Y") {
                $3=24
        } 
        if ($6 == "X") {
                $6=23
        } 
        else if ($6 =="Y") {
                $6=24
        }
        comp=comp+1 
        print $3 "\t" $4 "\t" $5 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 
    }
} ' $INPUT_FILE_L > $outDir/tmpL.$i.bed

awk -v ty=$i '
BEGIN {
	comp=0
} 
{
        if ($8 == ty) {
            if ($3 == "X") {
                    $3=23
            } 
            else if ($3 =="Y") {
                    $3=24
            } 
            if ($6 == "X") {
                    $6=23
            } 
            else if ($6 =="Y") {
                    $6=24
            }
            comp=comp+1 
            print $3 "\t" $4 "\t" $5 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 
        }
} ' $INPUT_FILE_M > $outDir/tmpM.$i.bed

if [ $INPUT_FILE_S != "none" ] ; then
	awk -v ty=$i '
	BEGIN {
		comp=0
	} 
	{
		if ($8 == ty) {
		if ($3 == "X") {
			$3=23
		} 
		else if ($3 =="Y") {
			$3=24
		} 
		if ($6 == "X") {
			$6=23
		} 
		else if ($6 =="Y") {
			$6=24
		}
		comp=comp+1 
		print $3 "\t" $4 "\t" $5 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 
		}
	} ' $INPUT_FILE_S > $outDir/tmpS.$i.bed
fi
echo "done"
echo "Mergeing different bin events"

te=$(wc -l  $outDir/tmpL.$i.bed  | cut -d\  -f1)
echo "$te events found in L"
te2=$(wc -l  $outDir/tmpM.$i.bed  | cut -d\  -f1)
echo "$te2 events found in M"
if [ $INPUT_FILE_S != "none" ] ; then
	te3=$(wc -l  $outDir/tmpS.$i.bed  | cut -d\  -f1)
	echo "$te3 events found in S"
else 
	te3="0"
fi

if [ $te != "0" ]
then
    if [ $te2 != "0" ]
    then
        $BEDTOOL/intersectBed -v -a $outDir/tmpM.$i.bed -b $outDir/tmpL.$i.bed  > $outDir/tmpM_notL.$i.bed
        te4=$(wc -l  $outDir/tmpM_notL.$i.bed  | cut -d\  -f1)
        echo "$te4 events found in M not in L"
        if [ $te4 != "0" ]
        then
            cat $outDir/tmpL.$i.bed $outDir/tmpM_notL.$i.bed | sort -k1,1n -k2,2n > $outDir/tmpLM.$i.bed
        else
            cat $outDir/tmpL.$i.bed > $outDir/tmpLM.$i.bed
        fi
    else 
        cat $outDir/tmpL.$i.bed > $outDir/tmpLM.$i.bed
    fi
    if [ $te3 != "0" ]
    then
        $BEDTOOL/intersectBed -v -a $outDir/tmpS.$i.bed -b $outDir/tmpLM.$i.bed  > $outDir/tmpS_notLM.$i.bed
        te5=$(wc -l  $outDir/tmpS_notLM.$i.bed  | cut -d\  -f1)
        echo "$te5 events found in S not in LM"
        if [ $te5 != "0" ]
        then
            cat $outDir/tmpLM.$i.bed $outDir/tmpS_notLM.$i.bed | sort -k1,1n -k2,2n > $outDir/tmpLMS.$i.bed
        else
            cat $outDir/tmpLM.$i.bed > $outDir/tmpLMS.$i.bed
        fi
    else 
        cat $outDir/tmpLM.$i.bed > $outDir/tmpLMS.$i.bed
    fi
    te7=$(wc -l  $outDir/tmpLMS.$i.bed  | cut -d\  -f1)
    echo "Merging $i generate $te7 events"
else
    if [ $te2 != "0" ]
    then
        if [ $te3 != "0" ]
        then
            $BEDTOOL/intersectBed -v -a $outDir/tmpS.$i.bed -b $outDir/tmpM.$i.bed  > $outDir/tmpS_notM.$i.bed
            te6=$(wc -l  $outDir/tmpS_notM.$i.bed  | cut -d\  -f1)
            echo "$te6 events found in S not in M (no L events)"
            if [ $te6 != "0" ]
            then
                cat $outDir/tmpM.$i.bed $outDir/tmpS_notM.$i.bed | sort -k1,1n -k2,2n > $outDir/tmpLMS.$i.bed
            else
                cat $outDir/tmpM.$i.bed > $outDir/tmpLMS.$i.bed
            fi
        else 
            cat $outDir/tmpM.$i.bed > $outDir/tmpLMS.$i.bed
        fi
        te7=$(wc -l  $outDir/tmpLMS.$i.bed  | cut -d\  -f1)
        echo "Merging $i  generate $te7 events"
    else
        if [ $te3 != "0" ]
        then
            cat $outDir/tmpS.$i.bed > $outDir/tmpLMS.$i.bed
            te7=$(wc -l  $outDir/tmpLMS.$i.bed  | cut -d\  -f1)
            echo "Merging $i  generate $te7 events"
        else 
            if [ -f $outDir/tmpLMS.$i.bed ]
            then
                rm $outDir/tmpLMS.$i.bed
            fi
            touch $outDir/tmpLMS.$i.bed
            echo "No $i found in: $INPUT_FILE_L ; $INPUT_FILE_M ; $INPUT_FILE_S  "
        fi
    fi
fi
echo "done"
done

echo "Merging DEL and DUP"
cat $outDir/tmpLMS.DEL.bed $outDir/tmpLMS.DUP.bed | sort -k1,1n -k2,2n > $outDir/tmpLMS.bed

te8=$(wc -l  $outDir/tmpLMS.bed  | cut -d\  -f1)
echo "Merging DEL and DUP generate $te8 events"

echo "done"

echo "Formating ouptut"
awk ' BEGIN { 
    print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T" 
}
{
    ch=$1
    chn=$1
    st=$2
    en=$3
    split($4,Info,",")
    me=Info[2]
    sa=Info[3]
    si=Info[4]
    ty=Info[5]
    rs=Info[6] 
    print me "\t" sa "\t" ch "\t" st "\t" en "\t" chn "\t" si "\t" ty "\t" rs 
}' $outDir/tmpLMS.bed > $OUTPUT_FILE

echo "done"

echo "cleaning temporary file"
rm testM/tmpL.DEL.bed   testM/tmpLM.DUP.bed   testM/tmpLMS.DUP.bed  testM/tmpM_notL.DEL.bed  testM/tmpS.DUP.bed testM/tmpL.DUP.bed   testM/tmpLMS.bed      testM/tmpM.DEL.bed    testM/tmpM_notL.DUP.bed  testM/tmpS_notLM.DEL.bed testM/tmpLM.DEL.bed  testM/tmpLMS.DEL.bed  testM/tmpM.DUP.bed    testM/tmpS.DEL.bed       testM/tmpS_notLM.DUP.bed

echo "done"