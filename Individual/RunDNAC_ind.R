## Automate the use of  DNAC for calling paired CNV in WGS
##remove old data
rm(list=ls())

#####################################
##Usage
usage=function(errM) {
	cat("\nUSAGE : RunDNAC.R [option] <Value>\n")
	cat("       -n        : BAM file\n")
	cat("       -o        : output file \n")
	cat("       -b        : bin size in bp - default 10000bp \n")
	cat("       -c        : chromosome - default 1 to 22 \n")
	cat("       -h        : this help\n\n")
	stop(errM)
}


##################################
## get args
ARG=commandArgs(trailingOnly = T)
## defult arg values
noF=""
out=""
binS=10000
ChrN=c("chr1","chr2","chr3","chr4","chr5","chr6","chr7","chr8","chr9","chr10","chr11","chr12","chr13","chr14","chr15","chr16","chr17","chr18","chr19","chr20","chr21","chr22")
cspec=FALSE
naC="1"
## get arg variables
for (i in 1:length(ARG)) {
	if (ARG[i] == "-n") {
		noF=ARG[i+1]
	} else if (ARG[i] == "-o") {
		out=ARG[i+1]
	}  else if (ARG[i] == "-b") {
		binS=as.numeric(ARG[i+1])
	}  else if (ARG[i] == "-c") {
		ChrN=ARG[i+1]
		naC=paste("2",ChrN,sep="_")
	} else if (ARG[i] == "-h") {
		usage(" ")
	}
}
## check arg consitency
if (!(file.exists(noF))) {
	usage(paste("Error : BAM file not found",noF,sep=" "))
}
if (out == "") {
	usage("Error : Output basename not specified")
}

##listfunction
lte=function(x) return(length(x))

## main
library(IRanges)
library(Biobase)
library(cn.mops)
library(DNAcopy)
library(snow)
library(intervals)
bamP=dirname(noF)
binMin=5
cat("step 1\n")
#ChrN=c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22")
BAMFiles=list.files(path=bamP , pattern=".sorted.dup.bam$",full.names=TRUE)
bamDataRanges=getReadCountsFromBAM(BAMFiles,WL=binS,mode="paired",refSeqName=ChrN)
cat("step 2\n")
dataF=as.data.frame(bamDataRanges)
#rm(bamDataRanges)
save(list = ls(all=TRUE), file = paste(".RData",naC,sep="."))
cat("step 3\n")
MeanC=mean(as.vector(as.numeric(dataF[,6])))
Chr=dataF[,1]
Pos=dataF[,2]
logR=log2(as.vector(as.numeric(dataF[,6]))/(MeanC))
rm(dataF)
save(list = ls(all=TRUE), file = paste(".RData",naC,sep="."))
cat("step 6\n")
CNA.object=CNA(logR,Chr,Pos, data.type="logratio",sampleid="CPCGC")
cat("step 7\n")
smoothed.CNA.object=smooth.CNA(CNA.object)
rm(CNA.object)
cat("step 8\n")
segment1=segment(smoothed.CNA.object, verbose=1)
rm(smoothed.CNA.object)
callOS=segment1$output[segment1$output[,1] == "Sample.1",]
save(list = ls(all=TRUE), file = paste(".RData",naC,sep="."))
tmp=rep(2,dim(callOS)[1])
tmp[as.numeric(callOS[,6]) <= log2(0.05)]=0
tmp[as.numeric(callOS[,6]) > log2(0.05) & as.numeric(callOS[,6]) <= log2(0.5)]=1
tmp[as.numeric(callOS[,6]) > log2(0.5) & as.numeric(callOS[,6]) <= log2(0.7)]=1.5
tmp[as.numeric(callOS[,6]) >= log2(1.3) & as.numeric(callOS[,6]) < log2(1.5)]=2.5
tmp[as.numeric(callOS[,6]) >= log2(1.5) & as.numeric(callOS[,6]) < log2(2)]=3
tmp[as.numeric(callOS[,6]) >= log2(2) & as.numeric(callOS[,6]) < log2(2.5)]=4
tmp[as.numeric(callOS[,6]) >= log2(2.5) & as.numeric(callOS[,6]) < log2(3)]=5
tmp[as.numeric(callOS[,6]) >= log2(3) & as.numeric(callOS[,6]) < log2(3.5)]=6
tmp[as.numeric(callOS[,6]) >= log2(3.5) & as.numeric(callOS[,6]) < log2(4)]=7
tmp[as.numeric(callOS[,6]) >= log2(4)]=8
out1=cbind(callOS,tmp)
save(list = ls(all=TRUE), file = paste(".RData",naC,sep="."))
cat("step 10\n")
write.table(out1,file=paste(out,"filtDNAC.txt",sep="."),quote=F,sep="\t",col.names=F,row.names=F)
cat("step 11\n")
save(list = ls(all=TRUE), file = paste(".RData",naC,sep="."))





