#!/bin/sh

#Usage filterBedResults.sh ftout_input($1) mappability_map($2) output($3new$3) prefix($4 new $4) CNVnTSProx($5 new $5)

#parms
BEDTOOL="/lb/project/mugqic/software/tools/BEDTools-Version-2.15.0/bin"
execPath=$(dirname $0)
INPUT_FILE=$1
EXCL_MAP=$2
GEN_POS=$3
DGV_MAP=$4
OUTPUT_FILE=$3
if [ $# -eq 10 ] 
then
	CNV_PROX=$5
else
	CNV_PROX="0"
fi
#cleaning
rm -f $4.tmpChrL.txt $4.tmpMAP.txt $OUTPUT_FILE.filteredSV.annotate.txt 

##ok
echo "Format mappability map ..."
awk ' BEGIN {
	comp=0
} 
NR > 1 {
	if ($1 == "X") {
		$1=23
	} 
	else if ($1 =="Y") {
		$1=24
	} 
	if ($1 != "MT") {
		chCK=index($1,"GL")
		if (chCK == 0) {
			print $1 "\t" $2 "\t" $3 "\t" $4
		}
	} 
} '  $EXCL_MAP | sort -k1,1n -k2,2n > $4.preMAP.txt

$BEDTOOL/mergeBed -d 15000 -i $4.preMAP.txt > $4.tmpMAP.txt


echo "done"



for i in "DEL" "INS" "INV"  "DUP" "TRA" 
do
#cleaning
rm -f $4.tmp.$i.OvProx.txt $4.tmp.$i.OvProx.Col.txt $4.tmp.$i.OvProx.Col.Mapp.txt $4.tmpfres.$i.txt $4.fres.$i.txt

##create result file by type of events
echo "Generate result file for $i ..."
grep $i $INPUT_FILE | awk ' BEGIN {
	comp=0
} 
{
	if ($3 == "X") {
		$3=23
	} 
	else if ($3 =="Y") {
		$3=24
	} 
	if ($6 == "X") {
		$6=23
	} 
	else if ($6 =="Y") {
		$6=24
	}
	comp=comp+1 
	if ($3 == $6) {
                if ($4 <= $5) {
                    print $3 "\t" $4 "\t" $5 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10
                }
                else {
                    print $3 "\t" $5 "\t" $4 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10
                }
	} 
	else {
		print $3 "\t" $4 "\t" $4+1 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10 "\n"  $6 "\t" $5 "\t" $5+1 "\t" comp "," $1 "," $2 "," $7 "," $8 "," $9 "," $10
	} 
} ' |  grep -v "MT" | grep -v "GL" | sort -k 1,1 -k2,2 -n > $4.tmp.$i.txt

echo "done"

te=$(wc -l  $4.tmp.$i.txt  | cut -d\  -f1)

if [ $te != "0" ]
then 
	if [ $CNV_PROX = "1" ]
	then
		echo "Checking for additional proximity of TumS DEL/DUP calls (5kb - option valid for DNAC) ..."
		$BEDTOOL/mergeBed -nms -d 20000 -i $4.tmp.$i.txt >  $4.tmp.$i.OvProx.txt
		## Collapse merge calls
		$execPath/CollapseSVC.py -f $4.$i.OvProx.txt -o $4.$i.OvProx.Col.us.txt
		sort -k1,1n -k2,2n $4.tmp.$i.OvProx.Col.us.txt | awk ' BEGIN {
			cpt=1
		}
		{
			split($4,num,",")
			print $1 "\t" $2 "\t" $3 "\t" cpt "," num[2] "," num[3] "," num[4] "," num[5] "," num[6] "," num[7]
			cpt=cpt+1
		} ' > $4.tmp.$i.OvProx.Col.txt
		echo "done"
	else 
    ##ok
	
		## merge similar calls that overlapp or that in proximity of 100bp 
		echo "Filter-out on call proximity for $i events ..."
		$BEDTOOL/mergeBed -nms -d 100 -i $4.tmp.$i.txt >  $4.tmp.$i.OvProx.txt
		
		## Collapse merge calls
		echo "... Collaps calls for $i events ..."
		$execPath/CollapseSVC.py -f $4.tmp.$i.OvProx.txt -o $4.tmp.$i.OvProx.Col.txt
		
		
		echo "done"
	fi
	## remove calls which overlapp (90%) a region in the mappability exclusion map 
	echo "Filter-out on call due to mappability for $i events ..."
	$BEDTOOL/intersectBed -v -f 0.90 -a $4.tmp.$i.OvProx.Col.txt -b $4.tmpMAP.txt  > $4.tmp.$i.OvProx.Col.Mapp.txt
	
	echo "done"
		
	
	## reformat
	echo "Reformat  $i events for output ..."
	awk ' {
		split($4,Info,",")
		print Info[1] "\t" $0
	} ' $4.tmp.$i.OvProx.Col.Mapp.txt |  sort -k 1,1 -n > $4.tmpfres.$i.txt
	awk ' 
	NR==1 {
		ch=$2
		chn=$2
		st=$3
		en=$4
		split($5,Info,",")
		num=$1
		me=Info[2]
		sa=Info[3]
		si=Info[4]
		ty=Info[5]
		rs=Info[6] 
	} 
	NR > 1 {
		if ($1 != num) { 
			print me "\t" sa "\t" ch "\t" st "\t" en "\t" chn "\t" si "\t" ty "\t" rs 
			ch=$2
			chn=$2
			st=$3
			en=$4
			split($5,Info,",")
			num=$1
			me=Info[2]
			sa=Info[3]
			si=Info[4]
			ty=Info[5]
			rs=Info[6] 
		}  
	} END {
		print me "\t" sa "\t" ch "\t" st "\t" en "\t" chn "\t" si "\t" ty "\t" rs "\t" os "\t" dgv "\t" gec "\t" ged "\t" geu "\t" rpm
	} ' $4.tmpfres.$i.txt > $4.fres.$i.txt
	
	
	echo "done"
	
	#merging result
	
	awk ' {
       		print $0
		} ' $4.fres.$i.txt >> $4.tmpRall.txt
	
   
    ###do the cleaning of tmp files in the loop
    #rm -f $4.tmp.$i.OvProx.txt $4.tmp.$i.OvProx.Col.txt $4.tmp.$i.OvProx.Col.Mapp.txt $4.tmp.$i.OvProx.Col.Mapp.Micro.txt $4.tmp.$i.OvProx.Col.Mapp.Micro.DGV.txt $4.tmp.$i.OvProx.Col.Mapp.Micro.DGV.Gene.txt $4.tmp.$i.OvProx.Col.Mapp.Micro.DGV.Gene.UTR.txt $4.tmp.$i.OvProx.Col.Mapp.Micro.DGV.Gene.UTR.RM.txt $4.tmp.utrOV.txt $4.tmp.RMP.txt $4.tmpfres.$i.txt

else
    echo "No $i found in $INPUT_FILE "
    rm -f $4.tmp.$i.txt
fi

done

# BEGIN { 
# 	print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup_N/T\tOnco_State\tDGV_hit\tGene_cover\tGene_disrupt\tGene_UTR\tRepeat_Masker_hit" 
# } 
#cordinate sort output

#reformat chr name
#generate output files local variable
awk ' BEGIN { 
	print "Method\tSample_BN\tchr1\tpos1\tpos2\tchr2\tsize\tSV_Format\tReadSup" 
} 
{
	if ($3 == 23) {
		$3="X"
		ml=1
	}
	else if ( $3 == 24) {
		$3="Y"
		ml=1
	}
	if ($6 == 23) {
		$6="X"
		ml=1
	}
	else if ( $6 == 24) {
		$6="Y"
		ml=1
	}
	if (ml == 1) {
		print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6 "\t" $7 "\t" $8 "\t" $9
	}
	else {
		print $0
	}
} ' $4.tmpRall.txt > $OUTPUT_FILE.filteredSV.annotate.txt 

###do the cleaning of tmp files outside the loop
#rm $4.tmpChrL.txt $UTR_POS $4.tmpMAP.txt $4.tmpTS.txt $4.tmpOther.txt $4.tmpOther.sorted.txt
