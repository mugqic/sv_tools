#!/bin/sh
# 

#Usage: mergeBrD.sh Workdir($1) output_basname($2) 

touch $2.breakD.ctx 
touch $2.breakD.bed
touch $1/jobs_output/breakD.out
touch $1/jobs_output/breakD_conf.out
for i in  `cat  $1/tmpbam/tmpchr.txt `
do
if [ $i == "1" ] ; then
	cat $2.breakD.$i.ctx >>  $2.breakD.ctx
else
	awk ' NR <= 6 {next} NR > 6 {print $0} ' $2.breakD.$i.ctx >>  $2.breakD.ctx
fi
cat $1/jobs_output/breakD_$i.out >> $1/jobs_output/breakD.output
cat $1/jobs_output/breakD_conf.$i.out >> $1/jobs_output/breakD_conf.out
cat $2.breakD.$i.bed >>  $2.breakD.bed
rm $2.breakD.$i.ctx
rm $2.breakD.$i.cfg
rm $2.breakD.$i.bed
rm $1/jobs_output/breakD_$i.out
rm $1/jobs_output/breakD_conf.$i.out

done

awk ' NR <= 6 {next} NR > 6 {print $0} ' $2.breakD.TR.ctx >>  $2.breakD.ctx
cat $1/jobs_output/breakD_TR.out >> $1/jobs_output/breakD.output
cat $2.breakD.TR.bed >>  $2.breakD.bed
rm $2.breakD.TR.ctx
rm $1/jobs_output/breakD_TR.out
rm $2.breakD.TR.bed

