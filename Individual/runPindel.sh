#!/bin/sh
# 
##2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com

##Usage: runPindel.sh RefFatstaDir($1) Chro($2) basename($3) breakDancerFile($4)

#parameters (see manual at https://trac.nbic.nl/pindel/wiki/UserManual):
  #Number of threads [12]
  #Window ref size (in million bases) [100]
  #Maximum size of structural variations to be detected (1=128, 2=512, 3=2,048, 4=8,092, 5=32,368, 6=129,472, 7=517,888, 8=2,071,552, 9=8,286,208) [5]
  #Report inversions [True]
  #Report_duplications [True]
  #Report_long_insertions [True]
  #Report_breakpoints [True]
  #Report_close_mapped_reads [False]
  #Only report inserted (NT) sequences in deletions greater than (bp) [50]
  #Only report inversions greater than (bp) [50]
  #Only consider reads as evidence if they map with more than this number of bases to the reference [60]
  #Only map part of a read to the reference genome if there are no other candidate positions with no more than the specified number of mismatches position [3]
  #At the point where the read is split into two, there should at least be this number of perfectly matching bases between read and reference [10]
  #Expected fraction of sequencing errors [0.05]
  #Only reads with fewer mismatches with the reference genome than this fraction will be considered [0.1]
  #Use breakDancer results to increase specificity and sensitivity [False]

if [ -f $4 ]; then
	echo "Pindel -f $1/byChro/chr$2.fa -i $3.Pindel.conf -c $2 -T 12 -w 100 -x 7 -n 50 -v 50 -d 50 -a 3 -m 10 -e 0.01 -Q $3.$2.PI_BrD.calls -b $4 -o $3.$2.Pindel"
	Pindel -f $1/byChro/chr$2.fa -i $3.Pindel.conf -c $2 -T 12 -w 100 -x 9 -n 50 -v 50 -d 60 -a 3 -m 10 -e 0.01 -b $4 -o $3.$2.Pindel
else
	echo " Pindel -f $1/byChro/chr$2.fa -i $3.Pindel.conf -c $2 -T 12 -w 100 -x 7 -n 50 -v 50 -d 50 -a 3 -m 10 -e 0.01 -o $3.$2.Pindel"
	Pindel -f $1/byChro/chr$2.fa -i $3.Pindel.conf -c $2 -T 12 -w 100 -x 9 -n 50 -v 50 -d 60 -a 3 -m 10 -e 0.01 -o $3.$2.Pindel
fi
