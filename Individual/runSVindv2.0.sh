#!/bin/sh
# 
##2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com
##This pipeline runs the detection of indel and structural variation on paired normal tumoral Whole genome sequence data
##need this bash located in the same directory than this script:
#	mergeBrD_ind.sh
#	RunDNAC_ind.R
#	runPindel.sh
#	mergePindelChro.sh


echo "-----------------------------------------------------------------------------"
echo "runSVcancer.sh: 2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com"
echo "This pipeline runs the detection of indel and structural variation "
echo "on paired normal/tumoral whole genome illumina hiseq sequence data"
echo "-----------------------------------------------------------------------------"

TUM_FILE=""
MET_TUM_FILE=""
NOR_FILE=""
MET_NOR_FILE=""
REF_DIR=""
OUTPUT_BASENAME=""
START_STEP="0"
LAST_STEP="0"
EXCL_MAP=""
GENE_CORD=""




####
function usage {

            
            echo "Usage: `basename $0` [option] <value>"
            echo ""
            echo "runSVindv1.2.sh - launches the pipeline to detect Structural Variations in Whole genome data"
            echo "Uses MOAB msub [required]"
            echo "samtools (needs to be in the path) [required]"
            echo ""
            echo "Options:"
            echo "-b                    bam file"
            echo "-m                    insert size metrics file"
            echo "-o                    output basename"
            echo "-r                    reference genome folder"
#             echo "-x                    exclusion map bed file"
#             echo "-i                    inclusion bed file"
#             echo "-g                    ccds gene coordinate bed file"
#             echo "-d                    DGV bed file"
#             echo "-a                    microsatelite bed file"
#             echo "-c                    repeatMasker bed file" 
            echo "-v                    version"
            echo "-s                    start step"
            echo "-e                    end step"
            echo "-h                    this help"
            echo "Steps:"
	    echo "			1: BreakDancer"
            echo "			2: DNAC"
	    echo "			3: Pindel"
# 	    echo "			4: filtering results"
# 	    echo "    	                5: generate bed track"

exit 0

}

while getopts “h:b:m:s:e:x:i:g:d:a:c:o:r:v” OPT
do
    case "$OPT" in
        h)
            usage
            exit 0
            ;;
        v)
            echo "`basename $0` version 1.2"
            exit 0
            ;;
        b) 
           BAM_FILE=$OPTARG
           ;;
        m)
           MET_FILE=$OPTARG
           ;;
        o)
           OUTPUT_BASENAME=$OPTARG
           ;;
        r)
           REF_DIR=$OPTARG
           ;;
        x)
           EXCL_MAP=$OPTARG
           ;;
        i)
           INCL_MAP=$OPTARG
           ;;
        g)
           GENE_CORD=$OPTARG
           ;;
	d)
           DGV_CORD=$OPTARG
           ;;
	a)
           MICRO_SAT_CORD=$OPTARG
           ;;
	c)
           REPEATM_CORD=$OPTARG
           ;;
        s) 
           START_STEP=$OPTARG
           ;;
        e)
           LAST_STEP=$OPTARG
           ;;
        
    esac

done


        if [[ -z $BAM_FILE ]] || [[ -z $MET_FILE ]] || [[ -z $REF_DIR ]] || [[ -z $OUTPUT_BASENAME ]]  
then
     echo "Given argument bam file:  $BAM_FILE\nmetrics file:  $MET_FILE\nref: $REF_DIR  \nout: $OUTPUT_BASENAME \nexculsion map file: $EXCL_MAP \ngene coordinate file: $GENE_CORD \n"
     usage
     exit 1
fi


# Start function 0

function function_0 {
     usage
     exit 1
}


##get general info
execPath=$(dirname $0)
workDir=$(dirname $OUTPUT_BASENAME)
baseName=$(basename $OUTPUT_BASENAME)
pidNum=$$

echo $pidNum

cd $workDir

#Look for files

if [ ! -f $REF_FASTA ];then 
 echo "reference fasta file $REF_FASTA not found."; exit 1;
fi

if [ ! -d $OUTPUT_DIR/ ]; then
        mkdir $OUTPUT_DIR
fi

if [ ! -d jobs_output/ ]; then
        mkdir jobs_output/
fi

if [ ! -d command_logs ]; then
        mkdir command_logs
fi

###env variables setting
SAMTOOLS_HOME="/lb/project/mugqic/software/tools/samtools/"
BED_TOOLS_HOME="/lb/project/mugqic/software/tools/BEDTools-Version-2.15.0/bin/"
JOB_OUT=$workDir/jobs_output/
CMD_LOG=$workDir/command_logs

BinS=$(grep -A 1 "MEDIAN"  $MET_FILE | awk ' NR == 2 {print $1} ') 
BinSD=$(grep -A 1 "MEDIAN"  $MET_FILE | awk ' NR == 2 {print $6} ') 
wait

echo "JOBs informations are provided in : runSVc.log.$pidNum"


##---------------
##PEM analysis
##---------------
function function_1 {


echo "step 1 - Running PEM analysis : BreakDancer"

logfile="$CMD_LOG/BreakDancer.log"

if [ -f $logfile ]; then
        rm $logfile
	rm $JOB_OUT/breakDT_conf.out
	rm $JOB_OUT/breakD_conf.out
	rm $JOB_OUT/breakDCF.out
	rm $JOB_OUT/breakD_TR.out
	rm $JOB_OUT/formatBrD.out 
	rm $JOB_OUT/mergeBrD.out
	rm $JOB_OUT/mergeBrD.out 
	rm $JOB_OUT/breakD.clean.out 
fi

echo "workdir : "${workDir}

rm $OUTPUT_BASENAME.breakD.*  $JOB_OUT/breakD_*

# make 3SD in T vs 2 in N
string="/lb/project/mugqic/software/SV_indel/breakdancer-1.1_2011_02_21/perl/bam2cfg.pl -s 1 -g -h -c 3 $BAM_FILE > $OUTPUT_BASENAME.break.cfg"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_conf.out -m ae -M mb.guillimin@gmail.com -N breakD_conf.$pidNum -V -l walltime=30:00:00 > $CMD_LOG/runSVc.log.$pidNum
  
string="breakDancer -t -c 3 -b 500 -r 2 -s 7 -m 1000000 -g $OUTPUT_BASENAME.breakD.TR.bed -d $OUTPUT_BASENAME.breakD.TR.ctx $OUTPUT_BASENAME.breakD.cfg > $OUTPUT_BASENAME.breakD.TR.ctx"
echo $string >> $logfile
echo $string
echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/breakD_TR.out -m ae -M mb.guillimin@gmail.com -N BrDa.$pidNum -V -l walltime=100:00:00,nodes=1:ppn=12,depend=breakD_conf.$pidNum > $CMD_LOG/runSVc.log.$pidNum


if [ ! -d  ${workDir}/tmpbam ]; then
	mkdir ${workDir}/tmpbam
fi

samtools view -H $BAM_FILE | grep "@SQ" | awk ' {print $2} ' | cut -d: -f 2 | grep -v "GL" | grep -v "MT" >  ${workDir}/tmpbam/tmpchr.txt

for i in  `cat  ${workDir}/tmpbam/tmpchr.txt `
do
rm  $JOB_OUT/breakD_stT_$i.out
rm $JOB_OUT/breakD_stN_$i.out
rm $JOB_OUT/breakD_conf.$i.out
rm $JOB_OUT/breakD_$i.out

#if no chrom bam:
string="samtools view -F 1024 -h -b $BAM_FILE $i > ${workDir}/tmpbam/$i.bam "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_stT_$i.out -N sp.$i.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=20:00:00 > $CMD_LOG/runSVc.log.$pidNum



string="sed \"s|$BAM_FILE|${workDir}/tmpbam/$i.bam|g\" $OUTPUT_BASENAME.breakD.cfg > $OUTPUT_BASENAME.breakD.$i.cfg"
 echo $string >> $logfile
 echo $string
 echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_conf.$i.out -N BrDa_conf.$i.$pidNum -V -m ae -M mb.guillimin@gmail.com -l depend=breakD_conf.$pidNum > $CMD_LOG/runSVc.log.$pidNum

string="breakDancer -c 3 -r 2 -m 1000000 -g $OUTPUT_BASENAME.breakD.$i.bed -d $OUTPUT_BASENAME.breakD.$i.ctx $OUTPUT_BASENAME.breakD.$i.cfg > $OUTPUT_BASENAME.breakD.$i.ctx "
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD_$i.out -N BrDa.$i.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=40:00:00,nodes=1:ppn=12 -l depend=BrDa_conf.$i.$pidNum:sp.$i.$pidNum > $CMD_LOG/runSVc.log.$pidNum

string="sed \"s|${workDir}/tmpbam/$i.bam|$BAM_FILE|g\" $OUTPUT_BASENAME.breakD.$i.ctx  > $OUTPUT_BASENAME.breakD.$i.ctx.tmp && mv $OUTPUT_BASENAME.breakD.$i.ctx.tmp $OUTPUT_BASENAME.breakD.$i.ctx"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/formatBrD.out -N BrDa_format.$pidNum -V -m ae -M mb.guillimin@gmail.com -l depend=BrDa.$i.$pidNum  > $CMD_LOG/runSVc.log.$pidNum

done


string="$execPath/mergeBrD_ind.sh ${workDir} $OUTPUT_BASENAME"
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/mergeBrD.out -N mergeBrD.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=10:00:00,depend=BrDa_format.$pidNum > $CMD_LOG/runSVc.log.$pidNum


# string="rm -rf $OUTPUT_BASENAME*.fastq " 
# echo $string >> $logfile
# echo $string
# echo $string| msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/breakD.clean.out -N BrDa.clean.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=3:00:00,depend=mergeBrD.$pidNum >> $CMD_LOG/runSVc.log.$pidNum

}


##---------------
##RD analysis
##---------------
# Start function 2

function function_2 {
 
echo "step 2 - Running RD analysis :DNAc"

logfile="$CMD_LOG/CNVnator.log"

if [ -f $logfile ]; then
       rm $logfile
fi
if [ ! -d ${workDir}/tmpbam2 ]; then
       mkdir ${workDir}/tmpbam2
fi

samtools view -H $BAM_FILE | grep "@SQ" | awk ' {print $2} ' | cut -d: -f 2 | grep -v "GL" >  ${workDir}/tmpbam/tmpchr.txt

# MAP_BAM="${workDir}/tmpbam2/map.bam"

# #remove low/hypper mappability region 
# string="samtools view -b -h -L $INCL_MAP $BAM_FILE > $MAP_BAM"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q amd -d ${workDir}/ -j oe -o $JOB_OUT/N.clean.out -N cleanMap.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=50:00:00,nodes=1:ppn=3 >>  $CMD_LOG/runSVc.log.$pidNum
# 
# string="samtools index $MAP_BAM"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q amd -d ${workDir}/ -j oe -o $JOB_OUT/N.index.out -N index.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=50:00:00,nodes=1:ppn=3,depend=cleanMap.$pidNum >>  $CMD_LOG/runSVc.log.$pidNum

#DNAC
string="Rscript_2.15 $execPath/RunDNAC_ind.R -n $BAM_FILE -o $OUTPUT_BASENAME.DNAC_10000"
echo $string >> $logfile
echo $string
echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/DNACfilt.1.out -M mb.guillimin@gmail.com -N DNACfilt.1.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=24:00:00,nodes=1:ppn=12 >>  $CMD_LOG/runSVc.log.$pidNum


for i in  `cat  ${workDir}/tmpbam/tmpchr.txt `
do
	string="Rscript_2.15 $execPath/RunDNAC_ind.R -n $BAM_FILE -o $OUTPUT_BASENAME.DNAC_500 -b 500 -c $i"
	echo $string >> $logfile
	echo $string
	echo $string | msub -q lm -d ${workDir}/ -j oe -o $JOB_OUT/DNACfilt.2.$i.out -M mb.guillimin@gmail.com -N DNACfilt.2.$pidNum -V -m ae -M mb.guillimin@gmail.com -l walltime=10:00:00,nodes=1:ppn=6 >>  $CMD_LOG/runSVc.log.$pidNum
done


}
##---------------
##SR analysis
##---------------

# Start function 3

function function_3 {
 
echo "step 3 - Running SR analysis : Pindel"

logfile="$CMD_LOG/Pindel.log"

if [ -f $logfile ]; then
        rm $logfile
fi


## generate normal-tumoral pair configuration file
######must be out-sourced in a specified bash
printf "$BAM_FILE\t$BinS\tSAMPLE\n" > $OUTPUT_BASENAME.Pindel.conf
wait

TESTD=$($execPath/jobTestMoab.sh mergeBrD.$pidNum)
##check dependency on breakDancer
if [ $TESTD == 0 ]; then
	L_LIST="walltime=52:00:00,nodes=1:ppn=12,depend=mergeBrD.$pidNum"
else
	L_LIST="walltime=52:00:00,nodes=1:ppn=12"
	if [ ! -d  ${workDir}/tmpbam ]; then
		mkdir ${workDir}/tmpbam
	fi
	samtools view -H $BAM_FILE | grep "@SQ" | awk ' {print $2} ' | cut -d: -f 2 | grep -v "GL" >  ${workDir}/tmpbam/tmpchr.txt
fi
##Pindel
#run Pindel chro by chro
mergeDep=""


for i in  `cat  ${workDir}/tmpbam/tmpchr.txt `
do
string="$execPath/runPindel.sh $REF_DIR $i $OUTPUT_BASENAME $OUTPUT_BASENAME.breakD.ctx " 
echo $string >> $logfile
echo $string
echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/PI.chr$i.out -N PI.chr$i.$pidNum -V -m ae -M mb.guillimin@gmail.com -l $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
mergeDep=$mergeDep:PI.chr$i.$pidNum 
done


#merge results, err and out files
echo "$execPath/mergePindelChro.sh $OUTPUT_BASENAME $JOB_OUT" | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/mergePI.out -m ae -M mb.guillimin@gmail.com -N mergePI.$pidNum -V -m ae -M mb.guillimin@gmail.com -l depend=$mergeDep >> $CMD_LOG/runSVc.log.$pidNum

}



##filterOut
# Start function 4

# function function_4 {
#  
# echo "step 4 - filtering out"
# 
# logfile="$CMD_LOG/filterOut.log"
# 
# if [ -f $logfile ]; then
#         rm $logfile
# fi
# TESTD=$($execPath/jobTestMoab.sh mergePI.$pidNum)
# ##check dependency on PI wich depends on breakdancer
# if [ $TESTD == 0 ]; then
# 	L_LIST="-l depend=mergePI.$pidNum:DNACfilt.1.$pidNum:DNACfilt.2.$pidNum"
# else
# 	L_LIST=""
# fi
# L_LIST=""
# 
# string=" $execPath/filterOutBrD.py -f $OUTPUT_BASENAME.breakD.ctx -n 10 -b $NOR_FILE -c $TUM_FILE -t 10 -o $OUTPUT_BASENAME.BrD -s $baseName "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterBD.out -m ae -M mb.guillimin@gmail.com -N filterBD.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# # 
# string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.BrD.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.BrD BD"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterBrD2.out -m ae -M mb.guillimin@gmail.com -N filterBrD2.$pidNum -V -m ae -M mb.guillimin@gmail.com -l depend=filterBD.$pidNum >> $CMD_LOG/runSVc.log.$pidNum
# # 
# string=" $execPath/filterOutPI.py -f $OUTPUT_BASENAME.Pindel -n 10 -t 10 -o $OUTPUT_BASENAME.PI -s $baseName "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterPI.out -m ae -M mb.guillimin@gmail.com -N filterPI.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST  >> $CMD_LOG/runSVc.log.$pidNum
# 
# string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.PI.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.PI PI"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterPI2.out -m ae -M mb.guillimin@gmail.com -N filterPI2.$pidNum -V -l depend=filterPI.$pidNum >> $CMD_LOG/runSVc.log.$pidNum
# 
# string="$execPath/filterOutDNAC.sh $OUTPUT_BASENAME.DNAC.filtDNAC.1.txt $OUTPUT_BASENAME.DNAC.1.filteredSV.txt $baseName 5"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC.1.out -m ae -M mb.guillimin@gmail.com -N filterDNAC.1.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string="$execPath/filterOutDNAC.sh $OUTPUT_BASENAME.DNAC.filtDNAC.2.txt $OUTPUT_BASENAME.DNAC.2.filteredSV.txt $baseName 5"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC.2.out -m ae -M mb.guillimin@gmail.com -N filterDNAC.2.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.DNAC.1.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.DNAC.1 DNAC 1"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC2.1.out -m ae -M mb.guillimin@gmail.com -N filterDNAC2.1.$pidNum -V -l depend=filterDNAC.1.$pidNum >> $CMD_LOG/runSVc.log.$pidNum
# 
# string="$execPath/filterBedResults.sh $OUTPUT_BASENAME.DNAC.2.filteredSV.txt $EXCL_MAP $GENE_CORD $DGV_CORD $MICRO_SAT_CORD $REPEATM_CORD $REF_DIR/chrLen.txt $OUTPUT_BASENAME.DNAC.2 DNAC 1"
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/filterDNAC2.2.out -m ae -M mb.guillimin@gmail.com -N filterDNAC2.2.$pidNum -V -l depend=filterDNAC.2.$pidNum:filterDNAC2.1.$pidNum >> $CMD_LOG/runSVc.log.$pidNum
# 
# }
# 
# ##generate Bed tracks and annotate
# # Start function 5
# 
# function function_5 {
#  
# echo "step 5 - Generate BED TRACK and annotate"
# 
# TESTD=$($execPath/jobTestMoab.sh filterPI2.$pidNum)
# ##check dependency on breakDancer
# if [ $TESTD == 0 ]; then
# 	L_LIST="-l depend=filterBrD2.$pidNum:filterPI2.$pidNum:filterPI2.$pidNum:filterDNAC2.1.$pidNum:filterDNAC2.2.$pidNum"
# else
# 	L_LIST=""
# fi
# 
# logfile="$CMD_LOG/bedAnnot.log"
# 
# if [ -f $logfile ]; then
#         rm $logfile
# fi
# 
# ##generate individuals germline tracks
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.BrD.other.filteredSV.annotate.txt $OUTPUT_BASENAME.BrD.other.filteredSV.annotate.bed "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDBrD.out -m ae -M mb.guillimin@gmail.com -N bedBrD.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.PI.other.filteredSV.annotate.txt $OUTPUT_BASENAME.PI.other.filteredSV.annotate.bed  "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDPI.out -m ae -M mb.guillimin@gmail.com -N bedPI.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC.1.other.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC.1.other.filteredSV.annotate.bed  "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC.out -m ae -M mb.guillimin@gmail.com -N bedDNAC.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# 
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC.2.other.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC.2.other.filteredSV.annotate.bed  "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC.out -m ae -M mb.guillimin@gmail.com -N bedDNAC.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# ##generate individuals tumSpe tracks
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.BrD.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.BrD.TumS.filteredSV.annotate.bed "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDBrD.out -m ae -M mb.guillimin@gmail.com -N bedBrD.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.PI.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.PI.TumS.filteredSV.annotate.bed  "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDPI.out -m ae -M mb.guillimin@gmail.com -N bedPI.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC.1.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC.1.TumS.filteredSV.annotate.bed  "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC2.out -m ae -M mb.guillimin@gmail.com -N bedDNAC.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# string=" $execPath/rtxt2rbed.sh $OUTPUT_BASENAME.DNAC.2.TumS.filteredSV.annotate.txt $OUTPUT_BASENAME.DNAC.2.TumS.filteredSV.annotate.bed  "
# echo $string >> $logfile
# echo $string
# echo $string | msub -q sw -d ${workDir}/ -j oe -o $JOB_OUT/BEDDNAC2.out -m ae -M mb.guillimin@gmail.com -N bedDNAC.$pidNum -V -m ae -M mb.guillimin@gmail.com $L_LIST >> $CMD_LOG/runSVc.log.$pidNum
# 
# 
# }



# Execute functions from $START_STEP to $LAST_STEP

for step in `seq $START_STEP $LAST_STEP`
do
function_$step
done

