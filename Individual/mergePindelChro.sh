#!/bin/sh
# 
##2011-09-20 - Mathieu Bourgey - mbourgey@genomequebec.com

##Usage: mergePindelChro.sh basenamePindelOutput($1) logout($2)

workDir=$(dirname $1)

rm $1.Pindel_BP
rm $1.Pindel_D
rm $1.Pindel_INV
rm $1.Pindel_LI
rm $1.Pindel_SI
rm $1.Pindel_TD
rm $2/PI.out

touch $1.Pindel_BP
touch $1.Pindel_D
touch $1.Pindel_INV
touch $1.Pindel_LI
touch $1.Pindel_SI
touch $1.Pindel_TD
touch $2/PI.out
for i in `cat  ${workDir}/tmpbam/tmpchr.txt `
do
cat $1.$i.Pindel_BP >> $1.Pindel_BP
cat $1.$i.Pindel_D >> $1.Pindel_D
cat $1.$i.Pindel_INV >> $1.Pindel_INV
cat $1.$i.Pindel_LI >> $1.Pindel_LI
cat $1.$i.Pindel_SI >> $1.Pindel_SI
cat $1.$i.Pindel_TD >> $1.Pindel_TD
cat $2/PI.chr$i.out >> $2/PI.out
rm $1.$i.Pindel_BP
rm $1.$i.Pindel_D
rm $1.$i.Pindel_INV
rm $1.$i.Pindel_LI
rm $1.$i.Pindel_SI
rm $1.$i.Pindel_TD
rm $2/PI.chr$i.out
done






